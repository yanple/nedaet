#!/bin/sh
pushd `dirname $0` > /dev/null
SCRIPTPATH=`pwd -P`
popd > /dev/null

compressor=${SCRIPTPATH}/yuicompressor-2.4.7.jar
project_dir=$(dirname "${SCRIPTPATH}")


STATIC="${project_dir}/static"
coffee=${STATIC}/coffee
js=${STATIC}/js
outfile=${js}/b/project.js

files="
    ${coffee}/utils.coffee
    ${coffee}/libs/emoticonize.coffee

    ${coffee}/example/store.coffee
    ${coffee}/example/model.coffee
    ${coffee}/example/template.coffee
    ${coffee}/example/view.coffee
    ${coffee}/example/controller.coffee
    ${coffee}/example/app.coffee

    ${coffee}/chat/store.coffee
    ${coffee}/chat/model.coffee
    ${coffee}/chat/template.coffee
    ${coffee}/chat/view.coffee
    ${coffee}/chat/controller.coffee
    ${coffee}/chat/app.coffee

	${coffee}/project.coffee
"

# Conctatenate and compile to JS
cat ${files} | coffee -m -sc > ${outfile}

# Minify JS
java -jar ${compressor} ${outfile} -o ${outfile} --nomunge



# Temp
# Find all *.coffee files, concatenate and compile to js
# find -L . -name *.coffee -exec cat {} \; | coffee -sc > static/js/build/darom.js