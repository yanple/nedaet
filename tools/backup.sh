#!/bin/sh

# Create dump media, database for user and download it to local.

### Config

user="liga"

server_backup_folder=backup   # Warning, this param is required! Not "/", else delete all files from user
local_backup_folder=~/Work/Yanple/backup
mega_backup_folder=/backup

### End Config

create_backup () {
    # receive one param: <username>
    username=$1

    dbname="$username"
    host="$username@yanple.com"

    echo -en "#################################################"
    echo -en "\n \033[1m \033[1;34m                Backup $username \033[0m \n"
    echo -en "#################################################\n"


    cur_date=`date +%d-%m-%Y`
    server_backup_folder_out=/home/"$username"/"$server_backup_folder"/"$cur_date"
    local_backup_folder_out="$local_backup_folder"/"$username"/
    mega_backup_folder_out="$mega_backup_folder"/"$username"/"$cur_date"

    # Dump database and media files
    echo "1. Connect to $host..."
    ssh "$host" "
        mkdir -p $server_backup_folder_out &&
        cd $server_backup_folder_out &&
        pg_dump -U $username $dbname > dump.sql
        echo 2. Dump created

        cd
        tar -czf $server_backup_folder_out/media.tar.gz media/
        echo 3. Media files archived

        echo 4. Uploading backup to Mega.co.nz
        ~/tools/./megacmd mkdir mega:$mega_backup_folder_out
        ~/tools/./megacmd sync $server_backup_folder_out mega:$mega_backup_folder_out

        echo 5. Delete old backups from server
        cd $server_backup_folder_out/..
        find . ! -path . -type d -not -path '*"$cur_date"*' | xargs rm -rf

        cd ..
    "

    # Download to local
#    echo "6. Start downloading backup to $local_backup_folder_out"
#    rsync -r -v -h --progress -e ssh "$host":"$server_backup_folder_out" "$local_backup_folder_out"

    echo -en "\n#################################################"
    echo -en "\n \033[1m \033[1;34m           Success backup for $username  \033[0m \n"
    echo -en "#################################################"
}

create_backup "$user"