# -*- coding: utf-8 -*-
from __future__ import unicode_literals

"""
This file was generated with the customdashboard management command and
contains the class for the main dashboard.

To activate your index dashboard add the following to your settings.py::
    GRAPPELLI_INDEX_DASHBOARD = 'project.dashboard.CustomIndexDashboard'
"""

from grappelli.dashboard import modules, Dashboard


class CustomIndexDashboard(Dashboard):
    """
    Custom index dashboard for www.
    """
    collapse_closed = ('grp-closed',)
    collapse_open = ('grp-open',)

    def init_with_context(self, context):

        self.children.append(modules.ModelList(
            'Чат',
            collapsible=True,
            column=1,
            css_classes=self.collapse_open,
            models=(
                'chat.models.Message',
            )
        ))
        self.children.append(modules.ModelList(
            'Личные сообщения',
            collapsible=True,
            column=1,
            css_classes=self.collapse_open,
            models=(
                'pm.models.Message',
            )
        ))

        self.children.append(modules.ModelList(
            'Управление пользователями',
            collapsible=True,
            column=1,
            css_classes=self.collapse_closed,
            models=('users.models.User',
                    'users.models.Group'
                    )
        ))

