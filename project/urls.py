from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = patterns('',
                       url(r'^grappelli/', include('grappelli.urls')),
                       url(r'^admin/', include(admin.site.urls)),

                       url(r'^', include('core.urls', namespace='core')),
                       url(r'^', include('authn.urls', namespace='authn')),
                       url(r'^users/', include('users.urls', namespace='users')),
                       url(r'^pass/', include('password.urls', namespace='password')),
                       url(r'^reg/', include('registration.urls', namespace='registration')),

                       url(r'^', include('chat.urls', namespace='chat')),

                       url(r'^pm', include('pm.urls', namespace='pm')),
                       )

urlpatterns += staticfiles_urlpatterns()

if settings.DEBUG:
    urlpatterns += patterns(
        '',
        url(r'^media/(?P<path>.*)$',
            'django.views.static.serve',
            {'document_root': settings.MEDIA_ROOT}))