# coding: utf-8
from fabric.api import env, run, cd

from project import settings

env.hosts = ['nedaet@yanple.com']
env.forward_agent = True

HOME = '~/'
VENV = HOME + 'venv/'
PYTHON = VENV + 'bin/python'
PIP = VENV + 'bin/pip'
MANAGE = PYTHON + ' manage.py'
MIGRATE = MANAGE + ' migrate'
PROJECT_PATH = HOME + 'django/'
UWSGI_TOUCH = HOME + '.uwsgi_reload'


def install_req():
    with cd(PROJECT_PATH):
        run('%s install -r requirements.txt' % PIP)


def git_pull():
    run('git pull')


def migrate(migrate_arg=''):
    with cd(PROJECT_PATH):
        run('%s %s' %
            (MIGRATE, migrate_arg))


def collect_static():
    with cd(PROJECT_PATH):
        run('%s collectstatic --noinput' % MANAGE)


def make_messages():
    with cd(PROJECT_PATH):
        for en, desc in settings.LANGUAGES:
            run('%s makemessages -l %s --ignore=venv' % (MANAGE, en))


def restart_uwsgi():
    run('touch %s' % UWSGI_TOUCH)
    print('All alright.')


def restart_visor():
    run("sudo supervisorctl restart nedaet_socket")
    print("Restart supervisor: Success")


def update(*args, **kwargs):
    with cd(PROJECT_PATH):
        git_pull()
    migrate(**kwargs)
    collect_static()
    restart_visor()
    restart_uwsgi()


def test():
    run('uname -a')
    print(env['user'])
