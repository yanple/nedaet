# coding: utf-8

from django import template
from django.utils.safestring import mark_safe

from core.utils import split_datetime

register = template.Library()


@register.filter
def class_path(value):
    if isinstance(value, object):
        return '%s.%s' % (value.__module__, value.__class__.__name__)
    return ''


@register.filter
def span_bell(value):
    """
    Surround bell symbol with span tag to prevent to convert email address
     to link.
    Example:
    user@example.com will be user<span>@</span>example.com
    """
    return mark_safe('<span>@</span>'.join(value.split('@')))


@register.filter()
def rm_nbsp(value):
    return mark_safe(" ".join(value.split('&nbsp;')))


def paginator(context, adjacent_pages=2):
    """
    Сокращение размера пагинации и её отображение.
    Например: 1 2 3 ... 9 10
    """
    paged = context['page_obj']
    paginator = paged.paginator
    start_page = max(paged.number - adjacent_pages, 1)
    if start_page <= 3:
        start_page = 1
    end_page = paged.number + adjacent_pages + 1
    if end_page >= paginator.num_pages - 1:
        end_page = paginator.num_pages + 1
    page_numbers = [n for n in range(start_page, end_page) if 0 < n <= paginator.num_pages]

    data = {
        'paged': paged,
        'paginator': paginator,
        'page': paged.number,
        'pages': paginator.num_pages,
        'page_numbers': page_numbers,
        'next': paged.next_page_number() if paged.has_next() else None,
        'previous': paged.previous_page_number() if paged.has_previous() else None,
        'has_next': paged.has_next(),
        'has_previous': paged.has_previous(),
        'show_first': 1 not in page_numbers,
        'show_last': paginator.num_pages not in page_numbers,
        'is_paginated': context['is_paginated'],

    }
    if 'request' in context:
        getvars = context['request'].GET.copy()
        if 'page' in getvars:
            del getvars['page']
        if len(getvars.keys()) > 0:
            data['getvars'] = "&%s" % getvars.urlencode()
        else:
            data['getvars'] = ''

    return data


register.inclusion_tag('core/_paginator.html', takes_context=True)(paginator)


@register.filter
def rupluralize(value, arg="дурак,дурака,дураков"):
    args = arg.split(",")
    number = abs(int(value))
    a = number % 10
    b = number % 100

    if (a == 1) and (b != 11):
        return args[0]
    elif (a >= 2) and (a <= 4) and ((b < 10) or (b >= 20)):
        return args[1]
    else:
        return args[2]


@register.filter
def get_split_value(field, index):
    value = field.value()
    if value:
        if isinstance(value, list):
            date = value[int(index)]
            if len(date):
                return date
            return ''
        else:
            return split_datetime(value, dashed=True)[int(index)]
    return ''
