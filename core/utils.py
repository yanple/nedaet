import re
import os
import json
import cgi
from uuid import uuid4
import cStringIO as StringIO
from datetime import tzinfo, timedelta
from lxml import etree

from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from django import http
from django.template.defaultfilters import strip_tags
from django.template.loader import get_template
from django.template import Context
from django.utils.dateformat import format as _dateformat, time_format as _timeformat
from django.http import HttpResponse
from django.utils.deconstruct import deconstructible


link_styles = {'style': 'color: #ff9900'}

ZERO = timedelta(0)


class UTC(tzinfo):
    def utcoffset(self, dt):
        return ZERO

    def tzname(self, dt):
        return "UTC"

    def dst(self, dt):
        return ZERO


utc = UTC()


class JsonResponse(HttpResponse):
    """
        JSON response
    """

    def __init__(self, content, mimetype='application/json', status=None, content_type=None):
        super(JsonResponse, self).__init__(
            content=json.dumps(content),
            mimetype=mimetype,
            status=status,
            content_type=content_type,
        )


def get_object_or_none(klass, *args, **kwargs):
    try:
        return klass._default_manager.get(*args, **kwargs)
    except klass.DoesNotExist:
        return None


def dateformat(date, dashed=False, human=False):
    if human:
        return _dateformat(date, 'd b, l')
    if dashed:
        return _dateformat(date, 'Y-m-d')
    return _dateformat(date, 'd.m.Y')


def timeformat(date):
    return _timeformat(date, 'H:i')


def datetimeformat(date, dashed=False, human=False):
    if human:
        return "%s %s" % (dateformat(date, human=True), timeformat(date))
    if dashed:
        return "%s %s" % (dateformat(date, dashed=True), timeformat(date))
    return "%s %s" % (_dateformat(date, 'd b Y'), timeformat(date))


def split_datetime(date, dashed=False):
    return [dateformat(date, dashed=dashed), timeformat(date)]


@deconstructible
class PathAndRename(object):
    def __init__(self, sub_path):
        self.path = sub_path

    def __call__(self, instance, filename):
        ext = filename.split('.')[-1]
        # set filename as random string
        filename = '{}.{}'.format(uuid4().hex, ext)
        # return the whole path to the file
        return os.path.join(self.path, filename)


def path_and_rename(url):
    return PathAndRename(url)


def stylize_email(text):
    """
    Parse html to stylize an elements
    """
    tree = etree.HTML(text)

    # stylish links
    for link in tree.xpath('//a[not(@style)]'):
        link.attrib.update(link_styles)

    return etree.tostring(tree)