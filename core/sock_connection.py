# coding: UTF-8
import json

from websocket import create_connection
from django.conf import settings


# from .sockjs_client import Client



# Connection to SockJS server

class ClientConnection(object):
    class Action:
        # new private message
        NEW_PM = "new_pm"

    sock = None

    def __init__(self, *args, **kwargs):
        super(ClientConnection, self).__init__()

        self.sock = create_connection("ws://%s:%s/%s/websocket" % (
            settings.SOCKJS_HOST,
            settings.SOCKJS_PORT,
            settings.SOCKJS_CHANNEL
        ))

    def send(self, action, data):
        ###
        #  data - dict object
        #  action - string
        ###
        if not data:
            return None

        msg = {
            "action": action,
            "pass": settings.SOCKJS_PASS,
            "data": data
        }

        self.sock.send(json.dumps(msg))


sock = ClientConnection()
