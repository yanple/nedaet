# coding=utf-8
from __future__ import unicode_literals

from django.forms import PasswordInput as DefaultPasswordInput

try:
    from django.utils.encoding import force_unicode as force_text
except ImportError:  # python3
    from django.utils.encoding import force_text


class PasswordInput(DefaultPasswordInput):
    def __init__(self, attrs=None, render_value=False):
        if attrs is None:
            attrs = {}
        attrs[
            'title'] = 'Для безопасности должен быть не меньше 6 символов, ' \
                       'содержать латинские буквы и цифры.'
        super(PasswordInput, self).__init__(attrs)
