# coding=utf-8
from __future__ import unicode_literals
from django.conf.urls import patterns, url
from authn.views import LoginView, LogoutView


urlpatterns = patterns('',
                       url(r'login$', LoginView.as_view(), name='login'),
                       url(r'logout$', LogoutView.as_view(), name='logout'),
                       )


