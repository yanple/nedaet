# coding=utf-8
from __future__ import unicode_literals
from urlparse import urlparse, urlunparse, urlsplit, urlunsplit
from django.contrib.auth import logout
from django.shortcuts import redirect
from django.views import generic
from authn.forms import LoginForm


class LoginView(generic.FormView):
    form_class = LoginForm
    template_name = 'authn/login.html'

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated() and not request.user.is_temp:
            return redirect('chat:chat')
        self.next = request.GET.get('next', '')
        return super(LoginView, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        redirect_url = self.request.POST.get('next')
        parts = urlsplit(redirect_url)
        new_next = '', '', parts.path, parts.query, parts.fragment
        # return redirect(urlunsplit(new_next) or self.request.user)
        return redirect(urlunsplit(new_next) or 'chat:chat')

    def get_form_kwargs(self):
        kwargs = super(LoginView, self).get_form_kwargs()
        kwargs.update(request=self.request)
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(LoginView, self).get_context_data(**kwargs)
        context.update(next=self.next)
        return context


class LogoutView(generic.TemplateView):
    template_name = 'authn/logout.html'

    def dispatch(self, request, *args, **kwargs):
        logout(request)
        return super(LogoutView, self).dispatch(request, *args, **kwargs)


