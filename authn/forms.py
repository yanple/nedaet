# coding=utf-8
from __future__ import unicode_literals
from django import forms
from django.contrib.auth import authenticate, login
from django.utils.safestring import mark_safe
from authn.utils import send_activation_url_mail


DEACTIVATED = 'Ваш аккаунт отключен,<br>' \
              '<a href="/Contacts/">обратитесь к администратору.</a>'

INACTIVE = 'Этот аккаунт не был активирован. Мы повторно послали Вам письмо' \
           ' с инструкцией по активации. Проверьте свой email.'


class LoginForm(forms.Form):
    username = forms.CharField(label='Логин', help_text='Пример: fedorpidor')
    password = forms.CharField(label='Пароль', widget=forms.PasswordInput())

    def __init__(self, request, *args, **kwargs):
        self.request = request
        super(LoginForm, self).__init__(*args, **kwargs)

    def clean(self):
        data = self.cleaned_data
        username = data.get('username', '').strip()
        password = data.get('password', '').strip()
        if password and username:
            user = authenticate(username=username, password=password)

            if not user:
                raise forms.ValidationError('Неправильное имя пользователя'
                                            ' или пароль.')
            if not user.is_active:
                if not user.activation_code:
                    # user deactivated by manager
                    raise forms.ValidationError(mark_safe(DEACTIVATED))
                else:
                    # resend activation link
                    send_activation_url_mail(user)
                    raise forms.ValidationError(mark_safe(INACTIVE))

            login(self.request, user)
        return data
