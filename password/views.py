from __future__ import unicode_literals
import os
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.contrib.sites.models import Site
from django.core.mail import EmailMessage
from django.core.urlresolvers import reverse
from django.http import Http404
from django.shortcuts import get_object_or_404, redirect
from django.template.loader import render_to_string
from django.utils.decorators import method_decorator
from django.views import generic
from password.forms import ChangePasswordForm, ResetForm, SetPasswordForm
from users.models import User


class ChangePassword(generic.FormView):
    form_class = ChangePasswordForm
    template_name = 'password/change.html'

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ChangePassword, self).dispatch(request, *args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super(ChangePassword, self).get_form_kwargs()
        kwargs.update(request=self.request)
        return kwargs

    def get_success_url(self):
        return reverse('users:update', args=[self.request.user.pk])


class ResetFormView(generic.FormView):
    form_class = ResetForm
    template_name = 'password/reset.html'

    def form_valid(self, form):
        cleaned_data = form.cleaned_data
        code = User.objects.make_random_password()
        user = get_object_or_404(User, email=cleaned_data.get('email'))
        user.reset_code = code
        user.save()
        context = dict(
            user=user,
            site=Site.objects.get_current()
        )

        subject = render_to_string('password/email_subject.txt',
                                   context).strip()
        body_html = render_to_string('password/email_body.html', context) \
            .strip()

        message = EmailMessage(subject, body_html,
                               settings.DEFAULT_FROM_EMAIL,
                               [user.email])
        message.content_subtype = 'html'
        message.send()

        return redirect('password:complete')


class ResetCompleteView(generic.TemplateView):
    template_name = 'password/complete.html'


class ResetDone(generic.TemplateView):
    template_name = 'password/done.html'


class ResetConfirm(generic.FormView):
    form_class = SetPasswordForm
    template_name = 'password/confirm.html'

    def dispatch(self, request, *args, **kwargs):
        self.user = get_object_or_404(User, pk=kwargs.get('pk'))
        code = kwargs.get('code')
        if not self.user.reset_code == code:
            raise Http404
        return super(ResetConfirm, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        cleaned_data = form.cleaned_data
        self.user.set_password(cleaned_data.get('password1'))
        self.user.reset_code = None
        self.user.save()
        return redirect('password:done')
