from django.conf.urls import patterns, url
from password.views import ChangePassword, ResetFormView, ResetCompleteView,\
    ResetDone, ResetConfirm


urlpatterns = patterns(
    '',
    url(r'^change$', ChangePassword.as_view(), name='change'),
    url(r'^reset$', ResetFormView.as_view(), name='reset'),
    url(r'^reset/complete$', ResetCompleteView.as_view(), name='complete'),
    url(r'^reset/done$', ResetDone.as_view(), name='done'),
    url(r'^reset/(?P<pk>\d+)/(?P<code>\w+)$', ResetConfirm.as_view(),
        name='confirm'),
)
