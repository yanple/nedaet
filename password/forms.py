# coding=utf-8
from __future__ import unicode_literals

from django import forms
from django.conf import settings

from users.models import User
from core.widgets import PasswordInput


class ChangePasswordForm(forms.Form):
    current_password = forms.CharField(label='Текущий пароль',
                                       widget=PasswordInput)
    new_password1 = forms.CharField(label='Новый пароль',
                                    min_length=settings.PASSWORD_MIN_LENGTH,
                                    widget=PasswordInput)
    new_password2 = forms.CharField(label='Повторите пароль',
                                    min_length=settings.PASSWORD_MIN_LENGTH,
                                    widget=PasswordInput)

    def __init__(self, request, *args, **kwargs):
        self.request = request
        super(ChangePasswordForm, self).__init__(*args, **kwargs)

    def clean_current_password(self):
        password = self.cleaned_data.get('current_password')
        if not self.request.user.check_password(password):
            raise forms.ValidationError('Введенный пароль не совпадает с'
                                        ' текущим')
        return password

    def clean(self):
        password1 = self.cleaned_data.get('new_password1')
        password2 = self.cleaned_data.get('new_password2')
        if password1 == password2:
            self.request.user.set_password(password1)
            self.request.user.save()
        else:
            raise forms.ValidationError('Новые пароли не совпадают')
        return self.cleaned_data


class ResetForm(forms.Form):
    email = forms.EmailField(
        label='Ваш email',
        help_text='Пример: ivanov@mail.ru',
        widget=forms.TextInput(attrs={
            'data-placement': 'right',
            'title': 'На данный e-mail будут отправлены инструкции по '
                     'восстановлению пароля.'}))

    def clean_email(self):
        email = self.cleaned_data['email']
        if not User.objects.filter(email=email).exists():
            raise forms.ValidationError('Пользователя с таким email не'
                                        ' существует.')
        return email


class SetPasswordForm(forms.Form):
    password1 = forms.CharField(
        widget=forms.PasswordInput,
        min_length=settings.PASSWORD_MIN_LENGTH,
        label='Новый пароль',
        help_text='Не менее'
                  ' %s символов' % settings.PASSWORD_MIN_LENGTH)
    password2 = forms.CharField(
        widget=forms.PasswordInput,
        min_length=settings.PASSWORD_MIN_LENGTH,
        label='Повторите пароль',
        help_text='Повторите')

    def clean(self):
        cleaned_data = super(SetPasswordForm, self).clean()
        password1 = cleaned_data.get('password1')
        password2 = cleaned_data.get('password2')
        if not password1 == password2:
            raise forms.ValidationError(
                'Пароли не совпадают. Оба введенных пароля должны'
                ' совпадать.')
        return cleaned_data
