# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import Message


class MessageAdmin(admin.ModelAdmin):
    list_display = ('user_name', 'text', 'created')

    def user_name(self, obj):
        return obj.user

    user_name.short_description = 'Пользователь'


admin.site.register(Message, MessageAdmin)