# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import json

from django.contrib.auth import get_user_model
from sockjs.tornado import SockJSConnection

from pm.models import Message as PMessage
from .utils import get_current_user, check_is_server
from .models import Message

User = get_user_model()


class ChatConnection(SockJSConnection):
    _connected = set()

    contacts = set()

    user = None
    is_server = False
    is_authenticated = False

    def on_open(self, info):
        self.user = get_current_user(info)

        if not self.user.is_authenticated():
            pass
        else:
            self.contacts.add(self.user)
            self.broadcast(self._connected, self.send_contact(self.user))

            self._connected.add(self)

            # send all messages
            messages = Message.objects \
                .raw('SELECT * '
                     'FROM (SELECT * FROM chat_message ORDER BY created DESC LIMIT 20) '
                     'AS users '
                     'ORDER BY created ASC')
            self.send(self.send_messages(messages))

            # show notify
            self.send(self.changed_notify_count(recipient=self.user.username))

            # send all contacts
            self.send(self.send_contacts(self.contacts))

            # notify all about new contact
            msg_enter = Message.objects.create(
                user=self.user,
                text="Присоединился к чату."
            )
            self.broadcast(self._connected, self.send_message(msg_enter))

    def on_message(self, data):
        data = json.loads(data)
        if self.is_server or check_is_server(data):
            self.server = True

            self.on_server_message(data)
            return None

        elif self.user.is_authenticated():
            msg = Message.objects.create(
                user=self.user,
                text=data['text']
            )
            self.broadcast(self._connected, self.send_message(msg))

    def on_close(self):
        self._connected.remove(self)
        self.contacts.remove(self.user)
        self.broadcast(self._connected, self.remove_contact(self.user))

        # notify all about leave contact
        msg_leave = Message.objects.create(
            user=self.user,
            text="Вышел из чата."
        )
        self.broadcast(self._connected, self.send_message(msg_leave))

    def on_server_message(self, data):
        action = data.get('action', None)
        if action == "new_pm":
            self.changed_notify_count(data=data)

    def send_message(self, m):
        # Send one message
        return {
            'type': 'new_message',
            'data': self._pack_msg(m)
        }

    def send_messages(self, queryset):
        # Send multiple message
        lst = [self._pack_msg(m) for m in queryset]
        return {
            'type': 'list_messages',
            'data': lst
        }

    def remove_contact(self, c):
        # Send one contact
        return {
            'type': 'remove_contact',
            'data': c.username
        }

    def send_contact(self, c):
        # Send one contact
        return {
            'type': 'new_contact',
            'data': self._pack_contact(c)
        }

    def send_contacts(self, contact_list):
        # Send multiple contact
        lst = [self._pack_contact(c) for c in contact_list]
        return {
            'type': 'list_contacts',
            'data': lst
        }

    def get_count_pm_unread(self, recipient):
        return PMessage.objects.filter(recipient__username=recipient,
                                       is_read=False).count()

    def changed_notify_count(self, recipient=None, data=None):
        if not recipient:
            recipient = data['data']['recipient']
        out = {
            'type': 'changed_notify_count',
            'data': self.get_count_pm_unread(recipient)
        }

        client = self.get_client_by_user(User.objects.get(username=recipient))
        clients = set()
        clients.add(client)
        self.broadcast(clients, out)

    def _pack_msg(self, m):
        return {
            'created': m.created.strftime('%H:%M:%S'),
            'text': m.text,
            'username': m.user.username,
            'id': m.id
        }

    def _pack_contact(self, c):
        return {
            'username': c.username,
            'avatar': c.get_avatar_40(),
            'about': c.get_short_about()
        }

    def get_client_by_user(self, user):
        for client in self._connected:
            print "iterate", client
            if client.user == user:
                return client
