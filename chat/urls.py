from django.conf.urls import patterns, url

from . import views

urlpatterns = patterns('chat.views',
                       url(r'^$', "chat", name='chat'),
                       url(r'^archive$', views.MessageList.as_view(), name='archive'),
                       )
