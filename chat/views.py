# coding=utf-8
from __future__ import unicode_literals

from django.contrib.auth import authenticate, login, get_user_model
from django.conf import settings
from django.shortcuts import render
from django.views import generic

User = get_user_model()

from .models import Message


def chat(request):
    if not request.user.is_authenticated():

        try:
            user = User.objects.create_user(
                is_temp=True
            )
            user.set_unusable_password()
            user.save()

            authenticate(user=user)
            login(request, user)
        except:
            pass

    return render(request, 'chat/chat.html', {
        'socket_port': settings.SOCKJS_PORT,
        'socket_channel': settings.SOCKJS_CHANNEL
    })


class MessageList(generic.ListView):
    model = Message
    paginate_by = 20

    def get_queryset(self):

        text = self.request.GET.get('q', None)

        if text:
            object_list = self.model.objects.filter(text__icontains=text)
        else:
            object_list = self.model.objects.all()
        return object_list
