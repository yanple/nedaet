# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.conf import settings

from sanitizer.models import SanitizedTextField

User = settings.AUTH_USER_MODEL


class Message(models.Model):
    user = models.ForeignKey(User, related_name="messages")
    text = SanitizedTextField(allowed_tags=['a', 'img', 'i', 'b'],
                              allowed_attributes=['href', 'src'],
                              strip=False)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = 'сообщение'
        verbose_name_plural = 'сообщения'.encode('utf-8')
        # ordering = ['created']

    def __unicode__(self):
        return self.text[:100]
