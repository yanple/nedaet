# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import django
from django.contrib.auth.models import AnonymousUser
from django.conf import settings
from django.contrib.auth import get_user_model

User = get_user_model()


def get_current_user(info):
    engine = django.utils.importlib.import_module(settings.SESSION_ENGINE)
    session_key = info.get_cookie(settings.SESSION_COOKIE_NAME)

    if not session_key:
        return AnonymousUser()

    session_key = str(session_key).split('=')[1]

    class Dummy(object):
        pass

    django_request = Dummy()
    django_request.session = engine.SessionStore(session_key)
    user = django.contrib.auth.get_user(django_request)
    return user


def check_is_server(data):
    if data.get('pass', None):
        if settings.SOCKJS_PASS == data['pass']:
            return True

    return False
