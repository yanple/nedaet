# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import sanitizer.models
from django.conf import settings


class Migration(migrations.Migration):
    dependencies = [
        ('pm', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='message',
            options={'ordering': ['created'], 'verbose_name': '\u0441\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u0435',
                     'verbose_name_plural': '\u043b\u0438\u0447\u043d\u044b\u0435 \u0441\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u044f'},
        ),
        migrations.AddField(
            model_name='message',
            name='subject',
            field=sanitizer.models.SanitizedCharField(max_length=100, null=True, verbose_name='\u0442\u0435\u043c\u0430'),
        ),
        migrations.AlterField(
            model_name='message',
            name='created',
            field=models.DateTimeField(auto_now_add=True,
                                       verbose_name='\u0434\u0430\u0442\u0430 \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f'),
        ),
        migrations.AlterField(
            model_name='message',
            name='recipient',
            field=models.ForeignKey(related_name='pm_recipient_messages',
                                    verbose_name='\u043f\u043e\u043b\u0443\u0447\u0430\u0442\u0435\u043b\u044c',
                                    to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='message',
            name='sender',
            field=models.ForeignKey(related_name='pm_sender_messages',
                                    verbose_name='\u043e\u0442\u043f\u0440\u0430\u0432\u0438\u0442\u0435\u043b\u044c',
                                    to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='message',
            name='text',
            field=sanitizer.models.SanitizedTextField(verbose_name='\u0442\u0435\u043a\u0441\u0442'),
        ),
    ]
