# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):
    dependencies = [
        ('pm', '0002_auto_20150517_1646'),
    ]

    operations = [
        migrations.AlterField(
            model_name='message',
            name='recipient',
            field=models.ForeignKey(related_name='pm_recipient_messages',
                                    verbose_name='\u043f\u043e\u043b\u0443\u0447\u0430\u0442\u0435\u043b\u044c',
                                    to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AlterField(
            model_name='message',
            name='sender',
            field=models.ForeignKey(related_name='pm_sender_messages',
                                    verbose_name='\u043e\u0442\u043f\u0440\u0430\u0432\u0438\u0442\u0435\u043b\u044c',
                                    to=settings.AUTH_USER_MODEL, null=True),
        ),
    ]
