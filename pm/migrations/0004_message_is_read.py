# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('pm', '0003_auto_20150517_1825'),
    ]

    operations = [
        migrations.AddField(
            model_name='message',
            name='is_read',
            field=models.BooleanField(default=False, verbose_name='\u043f\u0440\u043e\u0447\u0442\u0435\u043d\u043e?'),
        ),
    ]
