from django.conf.urls import patterns, url

from . import views

urlpatterns = patterns('pm.views',
                       url(r'^$', views.MessageList.as_view(), name='inbox'),
                       url(r'^/outbox$', views.MessageList.as_view(),
                           {'filter': 'outbox'}, name='outbox',
                           ),
                       url(r'^/all$', views.MessageList.as_view(),
                           {'filter': 'all'}, name='all', ),

                       url(r'^/new$', views.MessageCreate.as_view(), name='new', ),

                       url(r'^/new/(?P<username>\w+)$', views.MessageCreate.as_view(), name='new_to', ),

                       url(r'^/delete', 'delete_messages', name='delete',
                           ),
                       )
