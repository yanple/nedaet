# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import Message


class MessageAdmin(admin.ModelAdmin):
    list_display = ('sender_name', 'recipient_name', 'text', 'created')

    def sender_name(self, obj):
        return obj.sender

    sender_name.short_description = 'Отправитель'

    def recipient_name(self, obj):
        return obj.recipient

    recipient_name.short_description = 'Получатель'


admin.site.register(Message, MessageAdmin)
