# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.conf import settings

from sanitizer.models import SanitizedTextField, SanitizedCharField

User = settings.AUTH_USER_MODEL


class Message(models.Model):
    sender = models.ForeignKey('users.User', verbose_name='отправитель',
                               related_name="pm_sender_messages",
                               null=True)
    recipient = models.ForeignKey('users.User', verbose_name='получатель',
                                  related_name="pm_recipient_messages",
                                  null=True)

    subject = SanitizedCharField(verbose_name='тема', strip=False,
                                 max_length=100, null=True)

    text = SanitizedTextField(verbose_name='текст',
                              allowed_tags=['a', 'img'],
                              allowed_attributes=['href', 'src', 'width'],
                              strip=False)

    is_read = models.BooleanField('прочтено?', default=False)
    created = models.DateTimeField(auto_now_add=True, verbose_name='дата создания')

    class Meta:
        verbose_name = 'сообщение'
        verbose_name_plural = 'личные сообщения'.encode('utf-8')
        ordering = ['created']

    def __unicode__(self):
        return self.text[:100]
