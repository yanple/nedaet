# coding=utf-8
from __future__ import unicode_literals

from django import forms
from django.contrib.auth import get_user_model

from core.utils import get_object_or_none

User = get_user_model()

from .models import Message


class MessageForm(forms.ModelForm):
    recipient = forms.CharField(label='Ник получателя')
    subject = forms.CharField(label='Тема')
    text = forms.CharField(label='Текст сообщения', widget=forms.Textarea())

    class Meta:
        model = Message
        fields = ('recipient', 'subject', 'text')

    def clean_recipient(self):
        cleaned_data = self.cleaned_data
        recipient = cleaned_data.get('recipient')
        user = get_object_or_none(User, username=recipient)
        if not user:
            raise forms.ValidationError('Пользователя с таким ником не существует.')

        return user
