# coding=utf-8
from __future__ import unicode_literals

from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http import Http404
from django.shortcuts import redirect
from django.utils.decorators import method_decorator
from django.views import generic
from django.db.models import Q
from django.contrib import messages

User = get_user_model()

from core.sock_connection import sock

from .models import Message
from .forms import MessageForm


class MessageList(generic.ListView):
    model = Message
    paginate_by = 20

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        dispatcher = super(MessageList, self).dispatch(request, *args,
                                                       **kwargs)
        if request.user.is_temp:
            raise Http404

        return dispatcher

    def get_queryset(self):
        queryset = super(MessageList, self).get_queryset()

        filter = self.kwargs.get('filter', None)

        if filter == 'all':
            queryset = queryset.filter(Q(recipient=self.request.user) | Q(sender=self.request.user))

        elif filter == 'outbox':
            queryset = queryset.filter(sender=self.request.user)

        else:
            queryset = queryset.filter(recipient=self.request.user)

        self.change_to_read(queryset)

        return queryset

    def change_to_read(self, queryset):
        not_updated = queryset.filter(recipient=self.request.user, is_read=False)

        if not_updated:
            not_updated.update(is_read=True)


class MessageCreate(generic.CreateView):
    model = Message
    form_class = MessageForm

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        dispatcher = super(MessageCreate, self).dispatch(request, *args,
                                                         **kwargs)
        return dispatcher

    def get_form_kwargs(self):
        kwargs = super(MessageCreate, self).get_form_kwargs()
        if self.kwargs.get('username'):
            kwargs['initial'].update({
                'recipient': self.kwargs.get('username')
            })

        return kwargs

    def form_valid(self, form):
        if self.request.user == form.cleaned_data.get('recipient'):
            messages.error(self.request, 'И кому это ты собрался сообщение написать?')
            return redirect(reverse("pm:new"))

        message = form.save(commit=False)
        message.sender = self.request.user
        message.save()

        messages.success(self.request, 'Сообщение отправлено.')

        # notify recipient about new message
        sock.send(
            sock.Action.NEW_PM,
            {
                "recipient": message.recipient.username
            }
        )

        return redirect(reverse("pm:inbox"))


@login_required
def delete_messages(request):
    rp = request.POST
    if request.user.is_temp or not rp:
        raise Http404

    list_to_delete = rp.get('delete_messages', None)
    if delete_messages:
        Message.objects.filter(id__in=list_to_delete).delete()

    return redirect('pm:inbox')
