# coding=utf-8
from __future__ import unicode_literals

import xlrd

# from django.conf import settings
# from django.contrib.sites.models import Site
# from django.template.loader import render_to_string
# from mailer import send_html_mail


# def assign_manager_mail(user):
#     dct = {
#         'user': user,
#         'site': Site.objects.get_current(),
#     }

#     subject = render_to_string('managers/emails/assign_manager_subject.txt',
#         dct).strip()
#     body = render_to_string('managers/emails/assign_manager_body.html',
#         dct).strip()

#     send_html_mail(subject, '', body, settings.DEFAULT_FROM_EMAIL,
#         [user.manager.user.email])


# def mass_assign_to_manager_mail(users):
#     if not users:
#         return

#     dct = {
#         'users': users,
#         'site': Site.objects.get_current(),
#     }

#     subject = render_to_string('managers/emails/mass_assign_manager_subject.txt',
#         dct).strip()
#     body = render_to_string('managers/emails/mass_assign_manager_body.html',
#         dct).strip()

#     send_html_mail(subject, '', body, settings.DEFAULT_FROM_EMAIL,
#         [users[0].manager.user.email])


# def import_user_from_xls(xls):
#     from users.models import User, ManagerComment
#     count = {'created': 0, 'total': 0, 'unmodified': 0}
#     rb = xlrd.open_workbook(file_contents=xls)
#     sheet = rb.sheet_by_index(0)
#     for nrow in range(1, sheet.nrows):
#         cels = sheet.row_values(nrow)
#         comment = unicode(cels[0]).strip()
#         organization = unicode(cels[1]).strip()
#         fio = ' '.join(unicode(cels[2]).split(' ')[:3])
#         office = unicode(cels[3]).strip()
#         email = unicode(cels[4]).strip()
#         phone = unicode(cels[5]).strip()
#         address = unicode(cels[6]).strip()
#
#         fio_list = fio.split(' ')[:3]
#         last_name, first_name, middle_name = fio_list + [''] * (3 - len(fio_list))
#         user, created = User.objects.get_or_create(
#             email=email,
#             defaults=dict(
#                 last_name=last_name,
#                 first_name=first_name,
#                 middle_name=middle_name,
#                 phone=phone,
#                 address=address,
#                 office=office,
#                 organization=organization,
#                 is_active=True,
#             )
#         )
#
#         if comment:
#             ManagerComment.objects.create(user=user, text=comment)
#
#         if created:
#             count['created'] += 1
#         else:
#             count['unmodified'] += 1
#         count['total'] += 1
#     return count

def import_user_from_xls(xls):
    from users.models import User
    count = {'created': 0, 'total': 0, 'unmodified': 0}
    rb = xlrd.open_workbook(file_contents=xls)
    sheet = rb.sheet_by_index(0)
    for nrow in range(1, sheet.nrows):
        cels = sheet.row_values(nrow)
        email = unicode(cels[0]).strip()
        password = unicode(cels[1]).strip()
        fio = ' '.join(unicode(cels[2]).split(' ')[:3])
        organization = unicode(cels[3]).strip()
        city = unicode(cels[4]).strip()
        phone = unicode(cels[5]).strip().replace('.0', '')
        is_staff = unicode(cels[6]).strip()
        is_superuser = unicode(cels[7]).strip()

        if is_staff and is_staff.lower() == 'да':
            is_staff = True
        else:
            is_staff = False

        if is_superuser and is_superuser.lower() == 'да':
            is_superuser = True
            is_staff = True
        else:
            is_superuser = False

        fio_list = fio.split(' ')[:3]
        last_name, first_name, middle_name = fio_list + [''] * (3 - len(fio_list))
        user, created = User.objects.get_or_create(
            email=email,
            defaults=dict(
                last_name=last_name,
                first_name=first_name,
                middle_name=middle_name,
                phone=phone,
                organization=organization,
                city=city,
                is_staff=is_staff,
                is_superuser=is_superuser,
                is_active=True,
            )
        )
        user.set_password(password)
        user.save()

        if created:
            count['created'] += 1
        else:
            count['unmodified'] += 1
        count['total'] += 1
    return count
