# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import sanitizer.models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='user',
            name='middle_name',
        ),
        migrations.AddField(
            model_name='user',
            name='username',
            field=sanitizer.models.SanitizedCharField(max_length=30, unique=True, null=True, db_index=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='email',
            field=models.EmailField(help_text='\u041f\u0440\u0438\u043c\u0435\u0440: ivanov@mail.ru', unique=True, max_length=254, verbose_name='email'),
        ),
        migrations.AlterField(
            model_name='user',
            name='first_name',
            field=models.CharField(default='', max_length=64, null=True, verbose_name='\u0438\u043c\u044f', blank=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='last_name',
            field=models.CharField(default='', max_length=64, null=True, verbose_name='\u0444\u0430\u043c\u0438\u043b\u0438\u044f', blank=True),
        ),
    ]
