# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
import django.contrib.auth.models


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0006_require_contenttypes_0002'),
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(null=True, verbose_name='last login', blank=True)),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('email', models.EmailField(help_text='\u041f\u0440\u0438\u043c\u0435\u0440: ivanov@mail.ru', unique=True, max_length=254, verbose_name='email', db_index=True)),
                ('first_name', models.CharField(default='', max_length=64, verbose_name='\u0438\u043c\u044f', blank=True)),
                ('last_name', models.CharField(default='', max_length=64, verbose_name='\u0444\u0430\u043c\u0438\u043b\u0438\u044f', blank=True)),
                ('middle_name', models.CharField(default='', max_length=64, null=True, verbose_name='\u043e\u0442\u0447\u0435\u0441\u0442\u0432\u043e', blank=True)),
                ('phone', models.CharField(default='', help_text='\u041f\u0440\u0438\u043c\u0435\u0440: +79627334567', max_length=64, verbose_name='\u0442\u0435\u043b\u0435\u0444\u043e\u043d', blank=True)),
                ('city', models.CharField(default='', max_length=64, verbose_name='\u0433\u043e\u0440\u043e\u0434', blank=True)),
                ('is_active', models.BooleanField(default=False, verbose_name='\u0430\u043a\u0442\u0438\u0432\u043d\u044b\u0439')),
                ('is_staff', models.BooleanField(default=False, verbose_name='\u043f\u0435\u0440\u0441\u043e\u043d\u0430\u043b')),
                ('activation_code', models.CharField(max_length=32, null=True, verbose_name='\u043a\u043e\u0434 \u043f\u043e\u0434\u0442\u0432\u0435\u0440\u0436\u0434\u0435\u043d\u0438\u044f')),
                ('reset_code', models.CharField(max_length=32, null=True, verbose_name='\u043a\u043e\u0434 \u043f\u043e\u0434\u0442\u0432\u0435\u0440\u0436\u0434\u0435\u043d\u0438\u044f')),
                ('date_joined', models.DateTimeField(default=django.utils.timezone.now, verbose_name='\u0434\u0430\u0442\u0430 \u0440\u0435\u0433\u0438\u0441\u0442\u0440\u0430\u0446\u0438\u0438')),
                ('changed', models.DateTimeField(default=django.utils.timezone.now, verbose_name='\u0438\u0437\u043c\u0435\u043d\u0435\u043d')),
            ],
            options={
                'verbose_name': '\u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u044c',
                'verbose_name_plural': '\u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u0438',
                'permissions': (('view_user', 'Can view user profile'),),
            },
        ),
        migrations.CreateModel(
            name='Group',
            fields=[
            ],
            options={
                'verbose_name': '\u0433\u0440\u0443\u043f\u043f\u0430 \u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u0435\u0439',
                'proxy': True,
                'verbose_name_plural': '\u0433\u0440\u0443\u043f\u043f\u044b \u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u0435\u0439',
            },
            bases=('auth.group',),
            managers=[
                (b'objects', django.contrib.auth.models.GroupManager()),
            ],
        ),
        migrations.AddField(
            model_name='user',
            name='groups',
            field=models.ManyToManyField(related_query_name='user', related_name='user_set', to='auth.Group', blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', verbose_name='groups'),
        ),
        migrations.AddField(
            model_name='user',
            name='user_permissions',
            field=models.ManyToManyField(related_query_name='user', related_name='user_set', to='auth.Permission', blank=True, help_text='Specific permissions for this user.', verbose_name='user permissions'),
        ),
    ]
