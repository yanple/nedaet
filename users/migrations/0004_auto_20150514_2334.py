# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0003_user_is_temp'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='email',
            field=models.EmailField(help_text='\u041f\u0440\u0438\u043c\u0435\u0440: ivanov@mail.ru', max_length=254, unique=True, null=True, verbose_name='email'),
        ),
    ]
