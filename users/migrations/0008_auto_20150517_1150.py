# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations
import sanitizer.models


class Migration(migrations.Migration):
    dependencies = [
        ('users', '0007_auto_20150516_0119'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='user',
            name='city',
        ),
        migrations.RemoveField(
            model_name='user',
            name='last_name',
        ),
        migrations.RemoveField(
            model_name='user',
            name='phone',
        ),
        migrations.AddField(
            model_name='user',
            name='about',
            field=sanitizer.models.SanitizedTextField(null=True, blank=True),
        ),
    ]
