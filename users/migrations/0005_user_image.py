# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import sorl.thumbnail.fields
import core.utils


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0004_auto_20150514_2334'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='image',
            field=sorl.thumbnail.fields.ImageField(upload_to=core.utils.PathAndRename('avatars/'), null=True, verbose_name='\u0430\u0432\u0430\u0442\u0430\u0440', blank=True),
        ),
    ]
