# -*- coding: utf-8 -*-
# from __future__ import unicode_literals
import copy

from django.contrib import admin
from django.contrib.auth.models import Group as GroupOld
from django.contrib.auth.admin import GroupAdmin as GroupAdminOld
from django.contrib.admin.utils import flatten_fieldsets

from users.forms import ProfileAdminForm
from users.models import User, Group


class UserAdmin(admin.ModelAdmin):
    form = ProfileAdminForm
    list_display = ('get_full_name', 'email',
                    'is_active', 'is_temp', 'date_joined')
    list_display_links = ('get_full_name', 'email')
    list_filter = ('is_active', 'is_staff')
    filter_horizontal = ('groups', 'user_permissions')
    search_fields = ('first_name', 'email',)
    readonly_fields = ('last_login', 'date_joined')
    date_hierarchy = 'date_joined'
    list_per_page = 20
    ordering = ['-is_active', 'is_temp', 'date_joined']
    fieldsets = [
        (None, {
            'fields': [
                'username',
                'avatar',
                'email', 'first_name',
                'about',
                'date_joined', 'last_login']
        }),
        ('Изменение пароля пользователя', {
            'classes': ('grp-collapse grp-closed',),
            'fields': ('password1', 'password2')
        }),
    ]

    def save_model(self, request, obj, form, change):
        password = form.cleaned_data.get('password1')
        if password:
            obj.set_password(password)

        super(UserAdmin, self).save_model(request, obj, form, change)

    def get_fieldsets(self, request, obj=None):
        fieldsets = copy.deepcopy(self.declared_fieldsets)

        is_superuser = False if not obj else obj.is_superuser
        permission_fields = ['is_active']  # -- don't hide active checkbox, if not is_superuser else []
        if request.user.is_superuser:
            permission_fields += ['is_staff', 'is_superuser', 'groups', 'user_permissions']

        if permission_fields:
            fieldsets.insert(4, (
                'Дополнительные настройки групп и администрирования', {
                    'classes': ('grp-collapse grp-closed',),
                    'fields': permission_fields
                })
                             )

        return fieldsets

    def get_form(self, request, obj=None, **kwargs):
        return super(UserAdmin, self).get_form(request, obj, fields=flatten_fieldsets(self.get_fieldsets(request, obj)))


class GroupAdmin(GroupAdminOld):
    list_display = ('name', 'user_count')

    def user_count(self, obj):
        return obj.user_set.count()

    user_count.short_description = 'Количество пользователей'


admin.site.unregister(GroupOld)
admin.site.register(Group, GroupAdmin)
admin.site.register(User, UserAdmin)
