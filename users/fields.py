from django import forms
from users.models import User


class UserChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        if not isinstance(obj, User) and hasattr(obj, 'user'):
            obj = obj.user
        name = '%s %s' % (obj.last_name, obj.first_name)
        label = '%s (%s)' % (name.strip(), obj.email)
        return label.strip()
