# coding=utf-8
from __future__ import unicode_literals

from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http import Http404
from django.shortcuts import redirect, get_object_or_404
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.views import generic

from users.forms import ProfileForm
from users.models import User


class ProfileUpdate(generic.UpdateView):
    model = User
    form_class = ProfileForm

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        dispatcher = super(ProfileUpdate, self).dispatch(request, *args,
                                                         **kwargs)
        if request.user != self.object or request.user.is_temp:
            raise Http404

        return dispatcher

    def get_object(self, queryset=None):
        return get_object_or_404(User, username=self.kwargs.get('username', None))

    def form_valid(self, form):
        user = form.save(commit=False)
        user.changed = timezone.now()
        user.save()

        return redirect(reverse("chat:chat"))


class ProfileDetail(generic.DetailView):
    model = User

    def dispatch(self, request, *args, **kwargs):
        dispatcher = super(ProfileDetail, self).dispatch(request, *args,
                                                         **kwargs)
        if request.user.is_temp:
            raise Http404

        return dispatcher

    def get_object(self, queryset=None):
        return get_object_or_404(User, username=self.kwargs.get('username', None))
