# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import auth
from django.contrib.auth.models import (AbstractBaseUser, BaseUserManager,
                                        PermissionsMixin)
from django.core.urlresolvers import reverse
from django.db import models
from django.http.response import Http404
from django.utils import timezone
from django.utils.text import capfirst
from sorl.thumbnail import ImageField, get_thumbnail
from sanitizer.models import SanitizedTextField, SanitizedCharField

from core.utils import path_and_rename, get_object_or_none


class UserManager(BaseUserManager):
    def create_user(self, password=None, username=None, **kwargs):
        user = self.model(
            is_active=False,
            **kwargs
        )
        if password:
            user.set_password(password)

        user.save(using=self._db)

        if not username:
            username = "guest%s" % user.id

        if get_object_or_none(User, username=username):
            raise Http404

        user.username = username
        user.save(update_fields=['username'])

        return user

    def create_superuser(self, password, username, **kwargs):
        superuser = self.create_user(
            password=password,
            username=username,
            is_staff=True,
            is_superuser=True,
            **kwargs
        )
        superuser.is_active = True
        superuser.save()
        return superuser


class User(AbstractBaseUser, PermissionsMixin):
    username = SanitizedCharField('Логин', max_length=30,
                                  unique=True, db_index=True,
                                  null=True)

    email = models.EmailField('email', unique=True, null=True,
                              help_text='Пример: ivanov@mail.ru')

    # personal
    first_name = models.CharField('имя', max_length=64, default='',
                                  blank=True, null=True)

    avatar = ImageField('аватар', upload_to=path_and_rename('avatars/'),
                        null=True, blank=True)

    about = SanitizedTextField(allowed_tags=['a', 'img'],
                               allowed_attributes=['href', 'src', 'width'],
                               strip=False, null=True, blank=True)

    is_temp = models.BooleanField('временный пользователь', default=False)

    # system
    is_active = models.BooleanField('активный', default=False)
    is_staff = models.BooleanField('персонал', default=False)
    activation_code = models.CharField('код подтверждения', max_length=32,
                                       null=True)
    reset_code = models.CharField('код подтверждения', max_length=32,
                                  null=True)
    date_joined = models.DateTimeField('дата регистрации',
                                       default=timezone.now)
    changed = models.DateTimeField('изменен', default=timezone.now)

    objects = UserManager()

    USERNAME_FIELD = 'username'

    class Meta:
        verbose_name = 'пользователь'
        verbose_name_plural = 'пользователи'.encode('utf-8')
        ordering = ['-is_active', 'is_temp', 'date_joined']
        permissions = (
            ('view_user', 'Can view user profile'),
        )

    def __unicode__(self):
        return self.username

    @models.permalink
    def get_absolute_url(self):
        return 'users:detail', [self.pk]

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.first_name = capfirst(self.first_name.lower())
        super(User, self).save(force_insert, force_update, using,
                               update_fields)

    def get_admin_url(self):
        return reverse('admin:users_user_change', args=[self.id])

    def get_full_name(self):
        if self.first_name:
            return self.first_name
        return self.username

    get_full_name.short_description = 'полное имя'

    def get_short_name(self):
        if self.first_name:
            return self.first_name
        return self.username

    get_short_name.short_description = 'короткое имя'

    def get_activation_link(self):
        return reverse('registration:confirm',
                       args=(self.pk, self.activation_code))

    def get_avatar_40(self):
        if self.avatar:
            return get_thumbnail(self.avatar, '40x40', crop='center',
                                 format="PNG", quality=75).url
        else:
            return '/static/img/default/no_avatar.png'

    def get_short_about(self):
        if self.about:
            return self.about[:60]
        return None


class Group(auth.models.Group):
    class Meta:
        proxy = True
        verbose_name = 'группа пользователей'
        verbose_name_plural = 'группы пользователей'.encode('utf-8')
