# coding=utf-8
from __future__ import unicode_literals

from django.conf.urls import patterns, url

from users.views import ProfileUpdate, ProfileDetail

urlpatterns = patterns('',
                       url(r'^(?P<username>\w+)/edit$', ProfileUpdate.as_view(), name='update'),
                       url(r'^(?P<username>\w+)$', ProfileDetail.as_view(), name='detail'),
                       )
