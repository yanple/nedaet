# coding=utf-8
from __future__ import unicode_literals

import re

from django.core.exceptions import ValidationError
from django import forms

from .models import User


class ProfileForm(forms.ModelForm):
    about = forms.CharField(label='О себе', widget=forms.Textarea())

    username = forms.CharField(label='Ваш ник')
    avatar = forms.ImageField(label='Фото', required=False)

    class Meta:
        model = User
        fields = ('username', 'about', 'avatar')

    def clean_username(self):
        username = self.cleaned_data['username']
        users = User.objects.all()

        if re.search('^[a-z0-9_]+$', username):

            if username not in users.values_list('username', flat=True):

                if not re.match('^guest[\d]+$', username):
                    return username

                raise ValidationError('Такой ник только для гостей.')

            raise ValidationError('Этот ник уже занят.')

        raise ValidationError('Можно использовать только латинские символы, цифры и знак "_"')


class ProfileAdminForm(forms.ModelForm):
    password1 = forms.CharField(label='Пароль', required=False,
                                widget=forms.PasswordInput(
                                    attrs={'autocomplete': 'off'}))
    password2 = forms.CharField(label='Повторите пароль', required=False,
                                widget=forms.PasswordInput(
                                    attrs={'autocomplete': 'off'}))

    class Meta:
        model = User
        exclude = ('reset_code', 'activation_code')

    def clean_password2(self):
        cleaned_data = self.cleaned_data
        password1 = cleaned_data.get('password1')
        password2 = cleaned_data.get('password2')
        if password1 or password2:
            if not password1 == password2:
                raise forms.ValidationError('Введенные пароли не совпадают.')

    def clean_photo(self):
        photo = self.cleaned_data.get('photo')

        try:
            if photo and photo._size > 2 * 1024 * 1024:
                raise ValidationError("Фото должно быть не больше 2мб")
            return photo
        except AttributeError:
            pass
