# coding=utf-8
from __future__ import unicode_literals

from django.conf.urls import patterns, url

from registration.forms import RegistrationForm
from registration.views import (RegistrationFormView,
                                RegistrationCompleteView,
                                RegistrationConfirmView)


urlpatterns = patterns(
    '',

    url(r'^basic$',
        RegistrationFormView.as_view(form_class=RegistrationForm),
        name='basic'),

    url(r'^welcome$',
        RegistrationCompleteView.as_view(),
        name='complete'),

    url(r'^confirm/(?P<pk>\d+)/(?P<code>\w+)$',
        RegistrationConfirmView.as_view(),
        name='confirm'),
)
