# coding=utf-8
from __future__ import unicode_literals

import re

from django import forms
from django.utils.safestring import mark_safe
from django.core.exceptions import ValidationError

from users.models import User

ERROR_EMAIL_EXIST = '''
Пользователь с таким Email уже зарегистрирован,
 <a href="/authn/login/">войдите</a>
 '''


class RegistrationForm(forms.ModelForm):
    username = forms.CharField(label='Ваш ник')
    email = forms.EmailField(
        label='Email',
        help_text='Пример: ivanov@mail.ru',
        widget=forms.TextInput(
            attrs={'data-placement': 'right',
                   'title': 'На данный e-mail будут отправлены логин и пароль'}))

    about = forms.CharField(label='О себе', widget=forms.Textarea(), required=False)

    avatar = forms.FileField(label='Фото', required=False)

    class Meta:
        model = User
        fields = ('username', 'email', 'avatar', 'about')

    def clean_email(self):
        email = self.cleaned_data['email'].strip()
        if User.objects.filter(email__iexact=email).exists():
            raise forms.ValidationError(mark_safe(ERROR_EMAIL_EXIST))
        return email

    def clean_username(self):
        username = self.cleaned_data['username']
        users = User.objects.all()

        if re.search('^[a-z0-9_]+$', username):

            if username not in users.values_list('username', flat=True):

                if not re.match('^guest[\d]+$', username):
                    return username

                raise ValidationError('Такой ник только для гостей.')

            raise ValidationError('Этот ник уже занят.')

        raise ValidationError('Можно использовать только латинские символы, цифры и знак "_"')
