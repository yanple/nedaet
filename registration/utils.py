# coding=utf-8
from __future__ import unicode_literals

from django.conf import settings
from django.contrib.sites.models import Site
from django.core.mail import EmailMessage
from django.template.loader import render_to_string


def registration_mail(user, password):
    context = {
        'site': Site.objects.get_current(),
        'user': user,
        'password': password,
    }

    subject = render_to_string(
        'registration/email/registration_subject.html', context).strip()

    message_html = render_to_string(
        'registration/email/registration_body.html', context).strip()

    message = EmailMessage(subject, message_html, settings.DEFAULT_FROM_EMAIL,
                           [user.email])
    message.content_subtype = 'html'
    message.send()