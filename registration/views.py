# coding=utf-8
from __future__ import unicode_literals

from django.contrib import messages
from django.core.urlresolvers import reverse
from django.http import Http404
from django.shortcuts import redirect, get_object_or_404
from django.views import generic

from registration.forms import ERROR_EMAIL_EXIST, RegistrationForm
from registration.utils import registration_mail
from users.models import User


class RegistrationFormView(generic.CreateView):
    model = User
    form_class = RegistrationForm
    template_name = 'registration/form.html'

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated() and not request.user.is_temp:
            messages.info(request, 'Вы уже зарегистрированны.')
            return redirect(reverse('chat:chat'))

        return super(RegistrationFormView, self).get(request, *args, **kwargs)

    def form_valid(self, form):
        user = form.save()

        password = User.objects.make_random_password(length=16)
        user.set_password(password)
        user.activation_code = User.objects.make_random_password()
        user.save()

        registration_mail(user, password)

        return redirect(reverse('registration:complete'))

    def form_invalid(self, form):
        if ERROR_EMAIL_EXIST in form['email'].errors:
            messages.info(self.request, 'Вы уже зарегистриованны, войдите в систему.')
            return redirect(reverse('authn:login'))
        return super(RegistrationFormView, self).form_invalid(form)


class RegistrationCompleteView(generic.TemplateView):
    template_name = 'registration/complete.html'


class RegistrationConfirmView(generic.DetailView):
    model = User
    template_name = 'registration/confirm.html'

    def dispatch(self, request, *args, **kwargs):
        self.code = kwargs.get('code')
        self.user = get_object_or_404(User, pk=kwargs.get('pk'))

        if self.user.is_active or not self.user.activation_code:
            return redirect('authn:login')

        return super(RegistrationConfirmView, self).dispatch(request, *args,
                                                             **kwargs)

    def get_object(self, queryset=None):

        if not self.user.activation_code == self.code:
            raise Http404

        self.user.activation_code = None
        self.user.is_active = True
        self.user.save()
        messages.info(self.request, 'Аккаунт активирован. Теперь можете войти,'
                                    ' используя высланные логин и пароль.')

        return self.user

    def render_to_response(self, context, **response_kwargs):
        return redirect('authn:login')
