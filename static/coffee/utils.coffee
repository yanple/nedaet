#'use strict'
((window)->
  class Utils
    constructor: ->
      @init()


    init: ->
      FastClick.attach(document.body);

      if !browser.mozilla
        if ge("msgInput")
          @txaAutoSizeMsgInput(ge("msgInput"),
            geByClass1("chat_form"),
            ge("msgList")
          )

    test: ->
      debugLog "Utils: test"

    txaAutoSize: (el) ->
      height = el.offsetHeight
      diff = parseInt getStyle(el, "border-bottom")

      if trim(val(el)).length
        setStyle(el, "height", el.scrollHeight + diff + "px")

      addEvent el, "input keyup", (event) ->
        elEvent = event.srcElement
        currentScrollPosition = scrollGetY()
        setStyle(elEvent, "height", 0)
        setStyle(elEvent, "height", elEvent.scrollHeight + diff + "px")
        scrollToY(currentScrollPosition, 0)

    txaAutoSizeMsgInput: (el, panel_el, msg_list_el) ->
      height = el.offsetHeight
      diff = parseInt getStyle(el, "border-bottom")

      if trim(val(el)).length
        setStyle(panel_el, "height", el.scrollHeight + diff + 33 + "px")
        setStyle(msg_list_el, "paddingBottom", 96 + "px")

      addEvent el, "input keydown", (event) ->
        elEvent = event.target || event.srcElement

        currentScrollPosition = scrollGetY()

        setStyle(msg_list_el, "paddingBottom", 96 + "px")
        setStyle(msg_list_el, "paddingBottom", elEvent.scrollHeight + diff + 96 + "px")

        setStyle(panel_el, "height", 0)
        setStyle(panel_el, "height", elEvent.scrollHeight + diff + 33 + "px")
        scrollToY(currentScrollPosition, 0)

        bodyNode.scrollTop += 300

    replaceURLWithHTMLLinks: (text) ->
      exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
      return text.replace(exp, "<a href='$1'>$1</a>");


  window.Utils = Utils
  return)(window)