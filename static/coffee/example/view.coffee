#'use strict'
((window)->
  class View
    constructor: (@template) ->
      @$body = ge('body')
      @$page_body = ge('page_body')


    render: (viewCmd, parameter = {}) ->
      viewCommands =
        showContent: =>
          if ge "examContainer"
            re ge "examContainer"
          append(@$page_body, @template.showContent(parameter))

        showPagebar: =>
          if ge "pagebar"
            re ge "pagebar"
          append(@$page_body, @template.showPagebar(parameter))

        showNavbarDouble: =>
          parameter["double"] = true
          if ge("navbar")
            re(ge("navbar"))
          @$page_body.innerHTML = ""
          append(@$page_body, @template.showNavbar(parameter))

        showNavbar: =>
          parameter["double"] = false
          if ge("navbar")
            re(ge("navbar"))
          @$page_body.innerHTML = ""
          append(@$page_body, @template.showNavbar(parameter))

        showStart: =>
          if ge("examStart")
            re(ge("examStart"))
          append(@$page_body, @template.showStart(parameter))

        updateTimer: =>
          ge("examTimer").innerHTML = @template._humanTimer(parameter)

        showSendBtn: =>
          removeClass "examSendBtn", "__hidden"

        hideSendBtn: =>
          addClass "examSendBtn", "__hidden"


      viewCommands[viewCmd]()
      return


#    bind: (event, handler) ->
#      switch event
#        when "onResize"
#          eventFn = ->
#            handler()
#            return
#
#          addEvent window, "resize", eventFn
#          cur.destroy.push ->
#            removeEvent window, "resize", eventFn
#
#      return

    # Callable events from inline, like: onclick, onblur...

    getAnswerData: (handler) ->
      if ge "examAnswerForm"
        formData = new FormData(ge "examAnswerForm")
        formData.append "answer_text", val ge "examAnswerTextInput"

        answer_file = ge("examAnswerFileInput").files[0]
        if answer_file
          formData.append "answer_file", answer_file

        handler formData
        return
      else
        topError("Ошибка: не удается получить данные ответа.")



  window.Apps or= {}
  window.Apps.exam or= {}
  window.Apps.exam.View = View
  return)(window)