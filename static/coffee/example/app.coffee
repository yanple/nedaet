#'use strict'
((window)->
  class Exam
    constructor: (api_url) ->
      @s = new Apps.exam.Store(api_url)
      @m = new Apps.exam.Model(@s)
      @t = new Apps.exam.Template()
      @v = new Apps.exam.View(@t)
      @c = new Apps.exam.Controller(@m, @v)


  window.Exam = Exam
  return)(window)