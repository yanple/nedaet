#'use strict'
((window)->
  class Template
    constructor: (@storage) ->
      @contentTemplate =
      """
        <div class="cnt __sm __bg" id="examContainer">

          <div class="p-exam" id="examContent">

            <div class="p-exam_head">
              <div class="p-exam_head_question">
                #= data.text #
              </div>
            </div>
            <div class="p-exam_body">
              <button class="p-exam_body_btn
                  btn __raised __floated shadow-1 fr __hidden" id="examSendBtn"
                  onclick="app.exam.controller.sendAnswerBtnClick()">
                <i class="btn_ic mic-done"></i>
              </button>

              <div class="p-exam_body_answer">
                <form id="examAnswerForm" enctype="multipart/form-data">

                  <div class="minput p-exam_body_answer_txa">
                    <textarea name="answer_text" rows="1" class="mtxa" required="required"
                           type="text" id="examAnswerTextInput"
                           onkeyup="app.exam.controller.answerInputKeyup(this)"
                          ># if (data.answer_text) { # #= data.answer_text # # } #</textarea>
                    <span class="minput_highlight"></span>
                    <span class="minput_bar"></span>
                    <label>Введите ваш ответ *</label>
                  </div>
                  <div class="p-exam_body_answer_add">
                    <span class="p-exam_body_answer_add_t">
                      Добавить к ответу
                    </span>
                    <div class="p-exam_body_answer_add_a # if (data.answer_file) { # __active # } #"
                        onclick="app.exam.controller.addAnswerFileBtnClick(this)">
                      <i class="p-exam_body_answer_add_a_ic mic-insert-drive-file"></i> Файл (документ, изображение, аудио)
                      <input type="file" name="answer_file" id="examAnswerFileInput"
                         class="p-exam_body_answer_add_a_if">
                    </div>
                  </div>

                </form>
              </div>
            </div>

          </div>

        </div>
        """
      @pagebarTemplate =
      """
        <div class="p-pagebar" id="pagebar">
          <div class="p-pagebar_prev #if (data.current_question_index == 1){#__hidden#}#"
              onclick="app.exam.controller.prevQuestionBtnClick()">
            <i class="mic-chevron-left"></i>
          </div>

          <div class="p-pagebar_pages">
            <span class="p-pagebar_pages_curr">#= data.current_question_index #</span> из
            <span class="p-pagebar_pages_all">#= data.questions_count #</span>
          </div>

          <div class="p-pagebar_next #if (data.current_question_index == data.questions_count){#__hidden#}#"
              onclick="app.exam.controller.nextQuestionBtnClick()">
            <i class="mic-chevron-right"></i>
          </div>
        </div>
        """

      @startTemplate =
      """
      <div class="cnt __sm __bg __padding" id="examStart">

        <div class="p-test-start">

          <p class="p-test-start_count-questions">В тестировании #= data.questions_count #</p>

          <p class="p-test-start_info">Время на тест: #= data.time # мин.
          </p>
          <p class="p-test-start_instr">Инструкция: в качестве ответа загрузите файл, добавьте текстовый комментарий, если считаете нужным.
          </p>

          <button class="p-test-start_btn btn __primary __raised __colored shadow-1 center-block"
                onclick="app.exam.controller.startBtnClick()">
              Начать тест</button>
          <div class="clearfix"></div>
        </div>

      </div>
        """

      @navbarTemplate =
      """
        <div class="p-navbar # if (data.double) { #__double # } #" id="navbar">
          <a class="p-navbar_back" href="#= data.back_url #">
            <i class="mic-arrow-back"></i>
          </a>
          <ul class="p-navbar_t">
            <li class="p-navbar_t_i">
              <h4>Экзамен</h4>
            </li>
          </ul>

          # if (data.double) { #
            <div class="p-navbar_second">
              <p class="p-navbar_second_info">
                <i class="p-navbar_second_info_ic mic-access-alarms"></i>
                <span id="examTimer">#= data.timer #</span>
              </p>
            </div>
          # } #
        </div>
        """

    showContent: (data) ->
      tmpl = template @contentTemplate
      data.text = @_replaceNbsp(data.text)
      tmpl data

    showPagebar: (data) ->
      tmpl = template @pagebarTemplate
      tmpl data

    showNavbar: (data) ->
      tmpl = template @navbarTemplate
      data.timer = @_humanTimer(data.timer)
      tmpl data

    showStart: (data) ->
      tmpl = template @startTemplate
      tmpl @_pluralQuestionsCount data

    # Template filters

    _replaceNbsp: (text) ->
      str_replace "&nbsp;", " ", text

    _pluralQuestionsCount: (data) ->
      data.questions_count =
      """
        #{data.questions_count}
        #{pluralize data.questions_count, ["вопрос", "вопроса", "вопросов"]}
        """
      data

    _humanTimer: (seconds) ->
      minutes = 0
      if seconds >= 60
        minutes = parseInt(seconds / 60)

      seconds = seconds % 60
      """
        Осталось #{minutes} мин и #{seconds} сек
        """


  window.Apps or= {}
  window.Apps.exam or= {}
  window.Apps.exam.Template = Template
  return)(window)