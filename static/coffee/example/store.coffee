#'use strict'
((window)->
  class Store
    constructor: (@api_url, callback = ->) ->
      callback.call this # may be init AJAX call

    findExam: (params, callback) ->
      ###
      params:
        chapter: int
        id:      int (for single object)
      ###
      if params.id
        url = "#{@api_url}exams/#{params.id}?#{ajx2q params}"
      else
        url = "#{@api_url}exams?#{ajx2q params}"

      ajax.req
        url: url
        method: "get"
        headers:
          'Content-type': 'application/json'
        json: true
        done: (err, res) =>
          throw err if err
          callback.call this, res.json
          return

    createExamResult: (queryData, callback) ->
      ###
      queryData:
        quiz: int *
      ###
      ajax.req
        url: "#{@api_url}exams_results"
        method: "post"
        headers:
          'Content-type': 'application/x-www-form-urlencoded'
          'X-CSRFToken': yan.csrftoken
        json: true
        body: ajx2q(queryData)
        done: (err, res) ->
          throw err if err
          callback.call this, res.json
          return

      return

    findExamResult: (params, callback) ->
      ###
      params:
        id: int *
      ###
      ajax.req
        url: "#{@api_url}exams_results/#{params.id}"
        method: "get"
        headers:
          'Content-type': 'application/json'
        json: true
        done: (err, res) ->
          throw err if err
          callback.call this, res
          return

      return

    createExamResultAnswer: (formData, callback) ->
      ###
      formData:
        exam_question: int *
        exam_result:   int *
        answer_file:   file
        answer_text:   string
      ###

      ajax.req
        url: "#{@api_url}exams_results_answers"
        method: "post"
        headers:
          'X-CSRFToken': yan.csrftoken
        json: true
        body: formData
        done: (err, res) ->
          throw err if err
          callback.call this, res.json
          return

      return


  window.Apps or= {}
  window.Apps.exam or= {}
  window.Apps.exam.Store = Store
  return)(window)