#"use strict"
((window)->
  class Controller
    constructor: (@model, @view) ->
      @cache =
        exam: {}
        examResult: {}

      @current_question_index = 1
      @chapter

      # urls
      nav.add /ch\/(\d+)\/exam$/, =>
        @chapter = parseInt(arguments[0])
        @showStartPage(arguments[0])

      nav.add /ch\/(\d+)\/exam\/(\d+)$/, =>
        @chapter = parseInt(arguments[0])
        @showProcessPage(arguments[1])

    init: ->

      # events bind
#      @view.bind 'onResize', =>
#        @canvasResize()

    showStartPage: (chapter) ->
      @model.listExam chapter, (resp) =>
        if resp and resp.count >= 1
          examData = resp.results[0]

          # save to cache
          @cache.exam = examData

          # show Start page
          dataNavbar =
            back_url: "/ch/#{examData.chapter}"
          @view.render "showNavbar", dataNavbar

          dataStart =
            questions_count: examData.questions.length
            time: examData.time
          @view.render "showStart", dataStart
        else
          debugLog resp
          topMsg "Ошибка: Нет экзамена для этой главы"

    showProcessPage: (examResultId, exam = null) ->
      @_getExam {id: exam, chapter: @chapter}, (exam) =>
        @_getExamResult examResultId, (examResult) =>
          # check if time already exist
          if examResult.get_timer <= 0 or examResult.status == "finished"
            @_redirectToResult(examResultId)
            return

          # show navbar
          dataNavbar =
            back_url: "/ch/#{exam.chapter}"
            timer: examResult.get_timer
          @view.render "showNavbarDouble", dataNavbar

          # activate timer
          @_activateTimer examResult.get_timer

#          show content
          @view.render "showContent", exam.questions[0]

          # show pagebar
          dataPagebar =
            questions_count: exam.questions.length
            current_question_index: @current_question_index
          @view.render "showPagebar", dataPagebar

    start: ->
      @model.createExam @cache.exam.id, (resp) =>
        @cache.examResult = resp
        nav.go "ch/#{@chapter}/exam/#{resp.id}"

    sendAnswer: (finish=false) ->
      ###
      Get answer data, show next question and send answer in background.
      finish: bool (if true, then it will finish test)
      ###

      @view.getAnswerData (formData) =>
        curQuestion = @current_question_index  # copy for handler. If changed...

        # hide btn if last long request
        if finish or @cache.exam.questions.length == curQuestion
          @view.render "hideSendBtn"

        # add addition data for request
        formData.append "exam_question", @cache.exam.questions[curQuestion - 1].id
        formData.append "exam_result", @cache.examResult.id
        formData.append "finish", finish

        # show next question
        @nextQuestion()

        # send answer in background
        @model.createAnswer formData, (resp) =>
          status = resp.status

          if status == "finished"
            @_redirectToResult @cache.examResult.id
            return false
          else
            # save user answer in cache
            cur_question = @cache.exam.questions[curQuestion - 1]
            cur_question["answer_text"] = resp.answer_text
            cur_question["answer_file"] = resp.answer_file
            @cache.exam.questions[curQuestion - 1] = cur_question

    nextQuestion: ->
      questions = @cache.exam.questions
      if !(@current_question_index < questions.length)
        return

      # update questions
      @view.render "showContent", questions[@current_question_index]

      # increment @current_question_index
      @current_question_index += 1

      # update pagebar
      dataPagebar =
        questions_count: questions.length
        current_question_index: @current_question_index
      @view.render "showPagebar", dataPagebar

    prevQuestion: ->
      questions = @cache.exam.questions
      if !@current_question_index > 0
        return

      # increment @current_question_index
      @current_question_index -= 1

      # update questions
      @view.render "showContent", questions[@current_question_index - 1]

      # update pagebar
      dataPagebar =
        questions_count: questions.length
        current_question_index: @current_question_index
      @view.render "showPagebar", dataPagebar

    _getExam: (params, handler) ->
      ###
        Get exam from cache or server
        params:
          id:      int (exam id)
          chapter: int (chapter id)
      ###
      if !isEmpty(@cache.exam) and (@cache.exam.chapter == params.chapter or @cache.exam.id == params.id)
        handler @cache.exam
        return

      if params.id
        @model.getExam params.id, (resp) =>
          @cache.exam = resp
          handler @cache.exam
        return

      if params.chapter
        @model.listExam params.chapter, (resp) =>
          @cache.exam = resp.results[0]
          handler @cache.exam
        return

    _getExamResult: (id, handler) ->
      ###
        Get examResult from cache or server
      ###
      if !isEmpty(@cache.examResult) and @cache.examResult.id = id
        handler @cache.examResult
        return

      @model.getExamResult id, (resp) =>
        if resp.status != 200
            topMsg "Ошибка: этот тест не начат. Через 3 секунды, тест начнется сначала."
            setTimeout @_redirectToStart, 3000
            return
        @cache.examResult = resp.json
        handler @cache.examResult
      return

    _activateTimer: (seconds) ->
      @timer = seconds - 1  # decrement initial 1 second wait
      timeout = null
      activateTimer = =>
        timeout = setTimeout(=>
          @view.render "updateTimer", @timer
          if @timer <= 0
            @sendAnswer finish=true
          else
            @timer--
            activateTimer()
        , 1000)

      activateTimer()

      # destroy timeout
      cur.destroy.push ->
        clearTimeout timeout

    _redirectToResult: (resultId) ->
      window.location.replace "/ch/#{@chapter}/results/exam#{resultId}";

    _redirectToStart: =>
      window.location.replace "/ch/#{@chapter}/exam";

    # Callable events from inline, like: onclick, onblur...

    sendAnswerBtnClick: ->
      @sendAnswer()

    prevQuestionBtnClick: ->
      @prevQuestion()

    nextQuestionBtnClick: ->
      @nextQuestion()

    answerInputKeyup: (el) ->
      if trim(val el).length
        @view.render "showSendBtn"
      else
        @view.render "hideSendBtn"

    addAnswerFileBtnClick: (el) ->
      addClass el, "__active"

      ge("examAnswerFileInput").click()

    startBtnClick: ->
      @start()


  window.Apps or= {}
  window.Apps.exam or= {}
  window.Apps.exam.Controller = Controller
  return)(window)