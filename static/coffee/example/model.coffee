#'use strict'
((window)->
  class Model
    constructor: (@storage) ->

    getExam: (id, callback = ->) ->
      queryData =
        id: id

      @storage.findExam queryData, callback
      return

    listExam: (chapter, callback = ->) ->
      queryData =
        chapter: chapter

      @storage.findExam queryData, callback
      return

    createExam: (exam, callback = ->) ->
      queryData =
        exam: exam

      @storage.createExamResult queryData, callback
      return

    getExamResult: (id, callback = ->) ->
      queryData =
        id: id

      @storage.findExamResult queryData, callback
      return

    createAnswer: (formData, callback = ->) ->
      ###
      formData:
        exam_question: int *
        exam_result:   int *
        answer_file:   file
        answer_text:   string
      ###

      @storage.createExamResultAnswer formData, callback
      return


  window.Apps or= {}
  window.Apps.exam or= {}
  window.Apps.exam.Model = Model
  return)(window)