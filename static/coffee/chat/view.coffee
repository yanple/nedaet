#'use strict'
((window)->
  class View
    constructor: (@template) ->
      @$body = ge('body')
      @$page_body = ge('page_body')
      @$msgList = ge('msgList')
      @$msgInput = ge('msgInput')
      @$contactList = ge('contactList')
      @$notifyCount = ge('notifyCount')


    showMessages: (data) ->
      append(@$msgList, @template.showMessages(data))

      bodyNode.scrollTop += 10000

    showContacts: (data) ->
      append(@$contactList, @template.showContacts(data))

    removeContact: (username) ->
      if ge("contact_" + username)
        re "contact_" + username

    showNotifyCount: (count) ->
      if !count
        addClass(@$notifyCount, '__hide')
      else
        @$notifyCount.innerHTML = count
        removeClass(@$notifyCount, '__hide')

    addUserReply: (username) ->
      @clearMsgInput()
      @$msgInput.value = username


    showEmoticonize: (el_id) =>
      el = ge("msg#{el_id}")

      el.innerHTML = emoticonize el.innerHTML, true

    clearMsgInput: =>
      @$msgInput.value = ""

    newLineMsgInput: =>
      @$msgInput.value += "\n"


    #    bind: (event, handler) ->
    #      switch event
    #        when "onResize"
    #          eventFn = ->
    #            handler()
    #            return
    #
    #          addEvent window, "resize", eventFn
    #          cur.destroy.push ->
    #            removeEvent window, "resize", eventFn
    #
    #      return

    # Callable events from inline, like: onclick, onblur...

    getMessageData: (handler) ->
      msg = trim(@$msgInput.value)
      if msg.length
        handler msg
        return
      else
        topMsg("Ошибка: эм, а ты сообщение ввести не забыл?", 3)


  window.Apps or= {}
  window.Apps.chat or= {}
  window.Apps.chat.View = View
  return)(window)