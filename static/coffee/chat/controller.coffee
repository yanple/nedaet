#"use strict"
((window)->
  class Controller
    constructor: (@model, @view) ->
      @cache =
        chat: {}

      # urls

      nav.add /^$/, =>
        @init()

    init: ->
      if yan.user_is_auth
        @model.init @onMessage


    onMessage: (e) =>
      res = e.data
      if !res
        return

      type = res.type
      data = res.data
      console.log res.type

      switch type
        when 'new_message'
          @showMessage [data]
        when 'list_messages'
          @showMessages data

        when 'list_contacts'
          @showContacts data
        when 'new_contact'
          @showContacts [data]
        when 'remove_contact'
          @removeContact data

        when 'changed_notify_count'
          @change_notify_count(data)


    showMessage: (data) ->
      ###
         Show one message
      ###
      @view.showMessages data

    showMessages: (data) ->
      ###
         Show many messages
      ###
      @view.showMessages data

    removeContact: (username) ->
      ###
         Show one contact
      ###
      @view.removeContact username

    showContact: (data) ->
      ###
         Show one contact
      ###
      @view.showContacts data

    showContacts: (data) ->
      ###
         Show many contacts
      ###
      @view.showContacts data

    sendAnswer: () ->
      ###
      Get message data and send message.
      ###
      @view.getMessageData (msg) =>
        @model.msgCreate(msg)

        @view.clearMsgInput()

    change_notify_count: (count) ->
      @view.showNotifyCount count


# Callable events from inline, like: onclick, onblur...

    msgBtnSendClick: ->
      @sendAnswer()

    msgInputKeyDown: (e) ->
      ev = e || window.event;

      if ev.keyCode == 10 || ev.keyCode == 13 && (ev.ctrlKey || ev.metaKey && browser.mac)
        @view.newLineMsgInput()

      else if ev.keyCode == 13
        @sendAnswer()
        cancelEvent(e)

    msgUsernameClick: (el) ->
      username = el.innerHTML
      username = str_replace('[', '', username)
      username = str_replace(']', '', username)

      @view.addUserReply "#{username}, "


  window.Apps or= {}
  window.Apps.chat or= {}
  window.Apps.chat.Controller = Controller
  return)(window)