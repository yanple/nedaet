#'use strict'
((window)->
  class Model
    constructor: (@store) ->
      @sock

    init: (onMessage) ->
      # Init SockJS
      @store.init (socket) =>
        @sock = socket
      , onMessage

    msgCreate: (msg) ->
      msg =
        text: msg

      @store.send(msg)


  window.Apps or= {}
  window.Apps.chat or= {}
  window.Apps.chat.Model = Model
  return)(window)