#'use strict'
((window)->
  class Template
    constructor: (@storage) ->
      @msgTemplate =
      """
        {% for msg in it.message_list %}
          <div class="chat_messages_i" id="msg{{ msg.id }}">
              <div class="chat_messages_i_date">{{ msg.created }}</div>
              <div class="chat_messages_i_name"
                onclick="app.chat.c.msgUsernameClick(this);"
                >[{{ msg.username }}]</div>
              <div class="chat_messages_i_txt">
                  {{ msg.text }}
              </div>
              <div class="clearfix"></div>
          </div>
        {% endfor %}
        """

      @contactTeamplate =
      """
        {% for c in it.contact_list %}
          <div class="sidebar_contacts_i" id="contact_{{ c.username }}">
              <a class="sidebar_contacts_i_ava" href="/users/{{ c.username }}">
                  <img class="sidebar_contacts_i_ava_img"
                       src="{{ c.avatar }}"/>
              </a>

              <a class="sidebar_contacts_i_mail"
                 href="/pm/new/{{ c.username }}">
                  <i class="mic-mail"></i>
              </a>

              <div class="sidebar_contacts_i_body">
                  <a class="sidebar_contacts_i_name"
                    href="/users/{{ c.username }}"
                    >{{ c.username }}</a>
                  {% if c.about %}
                    <div class="sidebar_contacts_i_desc">{{ c.about }}</div>
                  {% endif %}
              </div>
          </div>
        {% endfor %}
        """


    showMessages: (data) ->
      tmpl = tpl.template @msgTemplate
      data = @stripText(data)
      data = @replaceLinks(data)
      data = @addBr(data)
      #      data = @markdown(data)

      cf tmpl {message_list: data}


    showContacts: (data) ->
      tmpl = tpl.template @contactTeamplate
      cf tmpl {contact_list: data}


# template filters

    addBr: (data) ->
      for obj in data
        obj.text = str_replace '\n', '<br/>', obj.text

      data

    replaceLinks: (data) ->
      for obj in data
        obj.text = app.utils.replaceURLWithHTMLLinks obj.text

      data

    stripText: (data) ->
      for obj in data
        obj.text = stripHTML obj.text

      data

  #    markdown: (data) ->
  #      for obj in data
  #        obj.text = micromarkdown.parse obj.text
  #
  #      data

  window.Apps or= {}
  window.Apps.chat or= {}
  window.Apps.chat.Template = Template
  return)(window)