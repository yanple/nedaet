#'use strict'
((window)->
  class Chat
    constructor: (echo_url) ->
      @s = new Apps.chat.Store(echo_url)
      @m = new Apps.chat.Model(@s)
      @t = new Apps.chat.Template()
      @v = new Apps.chat.View(@t)
      @c = new Apps.chat.Controller(@m, @v)


  window.Chat = Chat
  return)(window)