#'use strict'
((window)->
  class Store
    constructor: (@echo_url, callback = ->) ->
      callback.call this # may be init AJAX call
      @sock

    init: (onOpen, onMessage) ->
      # Connect to Echo
      @sock = new SockJS(@echo_url)

      @sock.onmessage = onMessage

      @sock.onclose = =>
        topError "Ошибка: соединение потеряно. Попробуйте обновить страницу."

      @sock.onopen = =>
        if @sock.readyState != SockJS.OPEN
          debugLog "chat: NOT connected"
        else
          debugLog 'chat: connected'

        onOpen.call this, @sock

    send: (data) ->
      ###*
        data (object or txt) *
      ###
      @sock.send JSON.stringify data


  window.Apps or= {}
  window.Apps.chat or= {}
  window.Apps.chat.Store = Store
  return)(window)