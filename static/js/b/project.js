(function () {
    var emoticonize, __bind = function (fn, me) {
        return function () {
            return fn.apply(me, arguments)
        }
    };
    (function (window) {
        var Utils;
        Utils = (function () {
            function Utils() {
                this.init()
            }

            Utils.prototype.init = function () {
                FastClick.attach(document.body);
                if (!browser.mozilla) {
                    if (ge("msgInput")) {
                        return this.txaAutoSizeMsgInput(ge("msgInput"), geByClass1("chat_form"), ge("msgList"))
                    }
                }
            };
            Utils.prototype.test = function () {
                return debugLog("Utils: test")
            };
            Utils.prototype.txaAutoSize = function (el) {
                var diff, height;
                height = el.offsetHeight;
                diff = parseInt(getStyle(el, "border-bottom"));
                if (trim(val(el)).length) {
                    setStyle(el, "height", el.scrollHeight + diff + "px")
                }
                return addEvent(el, "input keyup", function (event) {
                    var currentScrollPosition, elEvent;
                    elEvent = event.srcElement;
                    currentScrollPosition = scrollGetY();
                    setStyle(elEvent, "height", 0);
                    setStyle(elEvent, "height", elEvent.scrollHeight + diff + "px");
                    return scrollToY(currentScrollPosition, 0)
                })
            };
            Utils.prototype.txaAutoSizeMsgInput = function (el, panel_el, msg_list_el) {
                var diff, height;
                height = el.offsetHeight;
                diff = parseInt(getStyle(el, "border-bottom"));
                if (trim(val(el)).length) {
                    setStyle(panel_el, "height", el.scrollHeight + diff + 33 + "px");
                    setStyle(msg_list_el, "paddingBottom", 96 + "px")
                }
                return addEvent(el, "input keydown", function (event) {
                    var currentScrollPosition, elEvent;
                    elEvent = event.target || event.srcElement;
                    currentScrollPosition = scrollGetY();
                    setStyle(msg_list_el, "paddingBottom", 96 + "px");
                    setStyle(msg_list_el, "paddingBottom", elEvent.scrollHeight + diff + 96 + "px");
                    setStyle(panel_el, "height", 0);
                    setStyle(panel_el, "height", elEvent.scrollHeight + diff + 33 + "px");
                    scrollToY(currentScrollPosition, 0);
                    return bodyNode.scrollTop += 300
                })
            };
            Utils.prototype.replaceURLWithHTMLLinks = function (text) {
                var exp;
                exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
                return text.replace(exp, "<a href='$1'>$1</a>")
            };
            return Utils
        })();
        window.Utils = Utils
    })(window);
    emoticonize = function (text, showEmoticons) {
        var cssClass, emoticon, escapeCharacters, exclude, excludeArray, index, preMatch, specialCssClass, specialEmoticons, specialRegex, threeChar, threeCharacterEmoticons, twoChar, twoCharacterEmoticons, _i, _j, _k, _l, _len, _len1, _len2, _len3, _len4, _len5, _m, _n;
        escapeCharacters = [")", "(", "*", "[", "]", "{", "}", "|", "^", "<", ">", "\\", "?", "+", "=", "."];
        threeCharacterEmoticons = [":{)", ":-)", ":o)", ":c)", ":^)", ":-D", ":-(", ":-9", ";-)", ":-P", ":-p", ":-Þ", ":-b", ":-O", ":-/", ":-X", ":-#", ":'(", "B-)", "8-)", ";*(", ":-*", ":-\\", "?-)", ": )", ": ]", "= ]", "= )", "8 )", ": }", ": D", "8 D", "X D", "x D", "= D", ": (", ": [", ": {", "= (", "; )", "; ]", "; D", ": P", ": p", "= P", "= p", ": b", ": Þ", ": O", "8 O", ": /", "= /", ": S", ": #", ": X", "B )", ": |", ": \\", "= \\", ": *", ": &gt;", ": &lt;"];
        twoCharacterEmoticons = [":)", ":]", "=]", "=)", "8)", ":}", ":D", ":(", ":[", ":{", "=(", ";)", ";]", ";D", ":P", ":p", "=P", "=p", ":b", ":Þ", ":O", ":/", "=/", ":S", ":#", ":X", "B)", ":|", ":\\", "=\\", ":*", ":&gt;", ":&lt;"];
        specialEmoticons = {
            "&gt;:)": {cssClass: "red-emoticon small-emoticon spaced-emoticon"},
            "&gt;;)": {cssClass: "red-emoticon small-emoticon spaced-emoticon"},
            "&gt;:(": {cssClass: "red-emoticon small-emoticon spaced-emoticon"},
            "&gt;: )": {cssClass: "red-emoticon small-emoticon"},
            "&gt;; )": {cssClass: "red-emoticon small-emoticon"},
            "&gt;: (": {cssClass: "red-emoticon small-emoticon"},
            ";(": {cssClass: "red-emoticon spaced-emoticon"},
            "&lt;3": {cssClass: "pink-emoticon counter-rotated"},
            O_O: {cssClass: "no-rotate"},
            o_o: {cssClass: "no-rotate"},
            "0_o": {cssClass: "no-rotate"},
            O_o: {cssClass: "no-rotate"},
            T_T: {cssClass: "no-rotate"},
            "^_^": {cssClass: "no-rotate"},
            "O:)": {cssClass: "small-emoticon spaced-emoticon"},
            "O: )": {cssClass: "small-emoticon"},
            "8D": {cssClass: "small-emoticon spaced-emoticon"},
            XD: {cssClass: "small-emoticon spaced-emoticon"},
            xD: {cssClass: "small-emoticon spaced-emoticon"},
            "=D": {cssClass: "small-emoticon spaced-emoticon"},
            "8O": {cssClass: "small-emoticon spaced-emoticon"},
            "[+=..]": {cssClass: "no-rotate nintendo-controller"},
            OwO: {cssClass: "no-rotate"},
            "O-O": {cssClass: "no-rotate"},
            "O=)": {cssClass: "small-emoticon"}
        };
        specialRegex = new RegExp("(\\" + escapeCharacters.join("|\\") + ")", "g");
        preMatch = "(^|[\\s\\0])";
        for (index = _i = 0, _len = threeCharacterEmoticons.length; _i < _len; index = ++_i) {
            threeChar = threeCharacterEmoticons[index];
            threeChar = threeChar.replace(specialRegex, "\\$1");
            threeCharacterEmoticons[index] = new RegExp(preMatch + "(" + threeChar + ")", "g")
        }
        for (index = _j = 0, _len1 = twoCharacterEmoticons.length; _j < _len1; index = ++_j) {
            twoChar = twoCharacterEmoticons[index];
            twoChar = twoChar.replace(specialRegex, "\\$1");
            twoCharacterEmoticons[index] = new RegExp(preMatch + "(" + twoChar + ")", "g")
        }
        for (_k = 0, _len2 = specialEmoticons.length; _k < _len2; _k++) {
            emoticon = specialEmoticons[_k];
            emoticon.regexp = emoticon.replace(specialRegex, "\\$1");
            emoticon.regexp = new RegExp(preMatch + "(" + emoticon.regexp + ")", "g")
        }
        exclude = "span.css-emoticon";
        exclude += ",pre,code,.no-emoticons";
        excludeArray = exclude.split(",");
        if (!showEmoticons) {
            return text
        }
        cssClass = "css-emoticon";
        for (_l = 0, _len3 = specialEmoticons.length; _l < _len3; _l++) {
            emoticon = specialEmoticons[_l];
            specialCssClass = cssClass + " " + emoticon.cssClass;
            text = text.replace(emoticon.regexp, "$1<span class='" + specialCssClass + "'>$2</span>")
        }
        for (_m = 0, _len4 = threeCharacterEmoticons.length; _m < _len4; _m++) {
            threeChar = threeCharacterEmoticons[_m];
            text = text.replace(threeChar, "$1<span class='" + cssClass + "'>$2</span>")
        }
        for (_n = 0, _len5 = twoCharacterEmoticons.length; _n < _len5; _n++) {
            twoChar = twoCharacterEmoticons[_n];
            text = text.replace(twoChar, "$1<span class='" + cssClass + " spaced-emoticon'>$2</span>")
        }
        return text
    };
    (function (window) {
        var Store, _base;
        Store = (function () {
            function Store(api_url, callback) {
                this.api_url = api_url;
                if (callback == null) {
                    callback = function () {
                    }
                }
                callback.call(this)
            }

            Store.prototype.findExam = function (params, callback) {
                var url;
                if (params.id) {
                    url = "" + this.api_url + "exams/" + params.id + "?" + (ajx2q(params))
                } else {
                    url = "" + this.api_url + "exams?" + (ajx2q(params))
                }
                return ajax.req({
                    url: url,
                    method: "get",
                    headers: {"Content-type": "application/json"},
                    json: true,
                    done: (function (_this) {
                        return function (err, res) {
                            if (err) {
                                throw err
                            }
                            callback.call(_this, res.json)
                        }
                    })(this)
                })
            };
            Store.prototype.createExamResult = function (queryData, callback) {
                ajax.req({
                    url: "" + this.api_url + "exams_results",
                    method: "post",
                    headers: {"Content-type": "application/x-www-form-urlencoded", "X-CSRFToken": yan.csrftoken},
                    json: true,
                    body: ajx2q(queryData),
                    done: function (err, res) {
                        if (err) {
                            throw err
                        }
                        callback.call(this, res.json)
                    }
                })
            };
            Store.prototype.findExamResult = function (params, callback) {
                ajax.req({
                    url: "" + this.api_url + "exams_results/" + params.id,
                    method: "get",
                    headers: {"Content-type": "application/json"},
                    json: true,
                    done: function (err, res) {
                        if (err) {
                            throw err
                        }
                        callback.call(this, res)
                    }
                })
            };
            Store.prototype.createExamResultAnswer = function (formData, callback) {
                ajax.req({
                    url: "" + this.api_url + "exams_results_answers",
                    method: "post",
                    headers: {"X-CSRFToken": yan.csrftoken},
                    json: true,
                    body: formData,
                    done: function (err, res) {
                        if (err) {
                            throw err
                        }
                        callback.call(this, res.json)
                    }
                })
            };
            return Store
        })();
        window.Apps || (window.Apps = {});
        (_base = window.Apps).exam || (_base.exam = {});
        window.Apps.exam.Store = Store
    })(window);
    (function (window) {
        var Model, _base;
        Model = (function () {
            function Model(storage) {
                this.storage = storage
            }

            Model.prototype.getExam = function (id, callback) {
                var queryData;
                if (callback == null) {
                    callback = function () {
                    }
                }
                queryData = {id: id};
                this.storage.findExam(queryData, callback)
            };
            Model.prototype.listExam = function (chapter, callback) {
                var queryData;
                if (callback == null) {
                    callback = function () {
                    }
                }
                queryData = {chapter: chapter};
                this.storage.findExam(queryData, callback)
            };
            Model.prototype.createExam = function (exam, callback) {
                var queryData;
                if (callback == null) {
                    callback = function () {
                    }
                }
                queryData = {exam: exam};
                this.storage.createExamResult(queryData, callback)
            };
            Model.prototype.getExamResult = function (id, callback) {
                var queryData;
                if (callback == null) {
                    callback = function () {
                    }
                }
                queryData = {id: id};
                this.storage.findExamResult(queryData, callback)
            };
            Model.prototype.createAnswer = function (formData, callback) {
                if (callback == null) {
                    callback = function () {
                    }
                }
                this.storage.createExamResultAnswer(formData, callback)
            };
            return Model
        })();
        window.Apps || (window.Apps = {});
        (_base = window.Apps).exam || (_base.exam = {});
        window.Apps.exam.Model = Model
    })(window);
    (function (window) {
        var Template, _base;
        Template = (function () {
            function Template(storage) {
                this.storage = storage;
                this.contentTemplate = '<div class="cnt __sm __bg" id="examContainer">\n\n  <div class="p-exam" id="examContent">\n\n    <div class="p-exam_head">\n      <div class="p-exam_head_question">\n        #= data.text #\n      </div>\n    </div>\n    <div class="p-exam_body">\n      <button class="p-exam_body_btn\n          btn __raised __floated shadow-1 fr __hidden" id="examSendBtn"\n          onclick="app.exam.controller.sendAnswerBtnClick()">\n        <i class="btn_ic mic-done"></i>\n      </button>\n\n      <div class="p-exam_body_answer">\n        <form id="examAnswerForm" enctype="multipart/form-data">\n\n          <div class="minput p-exam_body_answer_txa">\n            <textarea name="answer_text" rows="1" class="mtxa" required="required"\n                   type="text" id="examAnswerTextInput"\n                   onkeyup="app.exam.controller.answerInputKeyup(this)"\n                  ># if (data.answer_text) { # #= data.answer_text # # } #</textarea>\n            <span class="minput_highlight"></span>\n            <span class="minput_bar"></span>\n            <label>Введите ваш ответ *</label>\n          </div>\n          <div class="p-exam_body_answer_add">\n            <span class="p-exam_body_answer_add_t">\n              Добавить к ответу\n            </span>\n            <div class="p-exam_body_answer_add_a # if (data.answer_file) { # __active # } #"\n                onclick="app.exam.controller.addAnswerFileBtnClick(this)">\n              <i class="p-exam_body_answer_add_a_ic mic-insert-drive-file"></i> Файл (документ, изображение, аудио)\n              <input type="file" name="answer_file" id="examAnswerFileInput"\n                 class="p-exam_body_answer_add_a_if">\n            </div>\n          </div>\n\n        </form>\n      </div>\n    </div>\n\n  </div>\n\n</div>';
                this.pagebarTemplate = '<div class="p-pagebar" id="pagebar">\n  <div class="p-pagebar_prev #if (data.current_question_index == 1){#__hidden#}#"\n      onclick="app.exam.controller.prevQuestionBtnClick()">\n    <i class="mic-chevron-left"></i>\n  </div>\n\n  <div class="p-pagebar_pages">\n    <span class="p-pagebar_pages_curr">#= data.current_question_index #</span> из\n    <span class="p-pagebar_pages_all">#= data.questions_count #</span>\n  </div>\n\n  <div class="p-pagebar_next #if (data.current_question_index == data.questions_count){#__hidden#}#"\n      onclick="app.exam.controller.nextQuestionBtnClick()">\n    <i class="mic-chevron-right"></i>\n  </div>\n</div>';
                this.startTemplate = '<div class="cnt __sm __bg __padding" id="examStart">\n\n  <div class="p-test-start">\n\n    <p class="p-test-start_count-questions">В тестировании #= data.questions_count #</p>\n\n    <p class="p-test-start_info">Время на тест: #= data.time # мин.\n    </p>\n    <p class="p-test-start_instr">Инструкция: в качестве ответа загрузите файл, добавьте текстовый комментарий, если считаете нужным.\n    </p>\n\n    <button class="p-test-start_btn btn __primary __raised __colored shadow-1 center-block"\n          onclick="app.exam.controller.startBtnClick()">\n        Начать тест</button>\n    <div class="clearfix"></div>\n  </div>\n\n</div>';
                this.navbarTemplate = '<div class="p-navbar # if (data.double) { #__double # } #" id="navbar">\n  <a class="p-navbar_back" href="#= data.back_url #">\n    <i class="mic-arrow-back"></i>\n  </a>\n  <ul class="p-navbar_t">\n    <li class="p-navbar_t_i">\n      <h4>Экзамен</h4>\n    </li>\n  </ul>\n\n  # if (data.double) { #\n    <div class="p-navbar_second">\n      <p class="p-navbar_second_info">\n        <i class="p-navbar_second_info_ic mic-access-alarms"></i>\n        <span id="examTimer">#= data.timer #</span>\n      </p>\n    </div>\n  # } #\n</div>'
            }

            Template.prototype.showContent = function (data) {
                var tmpl;
                tmpl = template(this.contentTemplate);
                data.text = this._replaceNbsp(data.text);
                return tmpl(data)
            };
            Template.prototype.showPagebar = function (data) {
                var tmpl;
                tmpl = template(this.pagebarTemplate);
                return tmpl(data)
            };
            Template.prototype.showNavbar = function (data) {
                var tmpl;
                tmpl = template(this.navbarTemplate);
                data.timer = this._humanTimer(data.timer);
                return tmpl(data)
            };
            Template.prototype.showStart = function (data) {
                var tmpl;
                tmpl = template(this.startTemplate);
                return tmpl(this._pluralQuestionsCount(data))
            };
            Template.prototype._replaceNbsp = function (text) {
                return str_replace("&nbsp;", " ", text)
            };
            Template.prototype._pluralQuestionsCount = function (data) {
                data.questions_count = "" + data.questions_count + "\n" + (pluralize(data.questions_count, ["вопрос", "вопроса", "вопросов"]));
                return data
            };
            Template.prototype._humanTimer = function (seconds) {
                var minutes;
                minutes = 0;
                if (seconds >= 60) {
                    minutes = parseInt(seconds / 60)
                }
                seconds = seconds % 60;
                return "Осталось " + minutes + " мин и " + seconds + " сек"
            };
            return Template
        })();
        window.Apps || (window.Apps = {});
        (_base = window.Apps).exam || (_base.exam = {});
        window.Apps.exam.Template = Template
    })(window);
    (function (window) {
        var View, _base;
        View = (function () {
            function View(template) {
                this.template = template;
                this.$body = ge("body");
                this.$page_body = ge("page_body")
            }

            View.prototype.render = function (viewCmd, parameter) {
                var viewCommands;
                if (parameter == null) {
                    parameter = {}
                }
                viewCommands = {
                    showContent: (function (_this) {
                        return function () {
                            if (ge("examContainer")) {
                                re(ge("examContainer"))
                            }
                            return append(_this.$page_body, _this.template.showContent(parameter))
                        }
                    })(this), showPagebar: (function (_this) {
                        return function () {
                            if (ge("pagebar")) {
                                re(ge("pagebar"))
                            }
                            return append(_this.$page_body, _this.template.showPagebar(parameter))
                        }
                    })(this), showNavbarDouble: (function (_this) {
                        return function () {
                            parameter["double"] = true;
                            if (ge("navbar")) {
                                re(ge("navbar"))
                            }
                            _this.$page_body.innerHTML = "";
                            return append(_this.$page_body, _this.template.showNavbar(parameter))
                        }
                    })(this), showNavbar: (function (_this) {
                        return function () {
                            parameter["double"] = false;
                            if (ge("navbar")) {
                                re(ge("navbar"))
                            }
                            _this.$page_body.innerHTML = "";
                            return append(_this.$page_body, _this.template.showNavbar(parameter))
                        }
                    })(this), showStart: (function (_this) {
                        return function () {
                            if (ge("examStart")) {
                                re(ge("examStart"))
                            }
                            return append(_this.$page_body, _this.template.showStart(parameter))
                        }
                    })(this), updateTimer: (function (_this) {
                        return function () {
                            return ge("examTimer").innerHTML = _this.template._humanTimer(parameter)
                        }
                    })(this), showSendBtn: (function (_this) {
                        return function () {
                            return removeClass("examSendBtn", "__hidden")
                        }
                    })(this), hideSendBtn: (function (_this) {
                        return function () {
                            return addClass("examSendBtn", "__hidden")
                        }
                    })(this)
                };
                viewCommands[viewCmd]()
            };
            View.prototype.getAnswerData = function (handler) {
                var answer_file, formData;
                if (ge("examAnswerForm")) {
                    formData = new FormData(ge("examAnswerForm"));
                    formData.append("answer_text", val(ge("examAnswerTextInput")));
                    answer_file = ge("examAnswerFileInput").files[0];
                    if (answer_file) {
                        formData.append("answer_file", answer_file)
                    }
                    handler(formData)
                } else {
                    return topError("Ошибка: не удается получить данные ответа.")
                }
            };
            return View
        })();
        window.Apps || (window.Apps = {});
        (_base = window.Apps).exam || (_base.exam = {});
        window.Apps.exam.View = View
    })(window);
    (function (window) {
        var Controller, _base;
        Controller = (function () {
            function Controller(model, view) {
                this.model = model;
                this.view = view;
                this._redirectToStart = __bind(this._redirectToStart, this);
                this.cache = {exam: {}, examResult: {}};
                this.current_question_index = 1;
                this.chapter;
                nav.add(/ch\/(\d+)\/exam$/, (function (_this) {
                    return function () {
                        _this.chapter = parseInt(arguments[0]);
                        return _this.showStartPage(arguments[0])
                    }
                })(this));
                nav.add(/ch\/(\d+)\/exam\/(\d+)$/, (function (_this) {
                    return function () {
                        _this.chapter = parseInt(arguments[0]);
                        return _this.showProcessPage(arguments[1])
                    }
                })(this))
            }

            Controller.prototype.init = function () {
            };
            Controller.prototype.showStartPage = function (chapter) {
                return this.model.listExam(chapter, (function (_this) {
                    return function (resp) {
                        var dataNavbar, dataStart, examData;
                        if (resp && resp.count >= 1) {
                            examData = resp.results[0];
                            _this.cache.exam = examData;
                            dataNavbar = {back_url: "/ch/" + examData.chapter};
                            _this.view.render("showNavbar", dataNavbar);
                            dataStart = {questions_count: examData.questions.length, time: examData.time};
                            return _this.view.render("showStart", dataStart)
                        } else {
                            debugLog(resp);
                            return topMsg("Ошибка: Нет экзамена для этой главы")
                        }
                    }
                })(this))
            };
            Controller.prototype.showProcessPage = function (examResultId, exam) {
                if (exam == null) {
                    exam = null
                }
                return this._getExam({id: exam, chapter: this.chapter}, (function (_this) {
                    return function (exam) {
                        return _this._getExamResult(examResultId, function (examResult) {
                            var dataNavbar, dataPagebar;
                            if (examResult.get_timer <= 0 || examResult.status === "finished") {
                                _this._redirectToResult(examResultId);
                                return
                            }
                            dataNavbar = {back_url: "/ch/" + exam.chapter, timer: examResult.get_timer};
                            _this.view.render("showNavbarDouble", dataNavbar);
                            _this._activateTimer(examResult.get_timer);
                            _this.view.render("showContent", exam.questions[0]);
                            dataPagebar = {questions_count: exam.questions.length, current_question_index: _this.current_question_index};
                            return _this.view.render("showPagebar", dataPagebar)
                        })
                    }
                })(this))
            };
            Controller.prototype.start = function () {
                return this.model.createExam(this.cache.exam.id, (function (_this) {
                    return function (resp) {
                        _this.cache.examResult = resp;
                        return nav.go("ch/" + _this.chapter + "/exam/" + resp.id)
                    }
                })(this))
            };
            Controller.prototype.sendAnswer = function (finish) {
                if (finish == null) {
                    finish = false
                }
                return this.view.getAnswerData((function (_this) {
                    return function (formData) {
                        var curQuestion;
                        curQuestion = _this.current_question_index;
                        if (finish || _this.cache.exam.questions.length === curQuestion) {
                            _this.view.render("hideSendBtn")
                        }
                        formData.append("exam_question", _this.cache.exam.questions[curQuestion - 1].id);
                        formData.append("exam_result", _this.cache.examResult.id);
                        formData.append("finish", finish);
                        _this.nextQuestion();
                        return _this.model.createAnswer(formData, function (resp) {
                            var cur_question, status;
                            status = resp.status;
                            if (status === "finished") {
                                _this._redirectToResult(_this.cache.examResult.id);
                                return false
                            } else {
                                cur_question = _this.cache.exam.questions[curQuestion - 1];
                                cur_question.answer_text = resp.answer_text;
                                cur_question.answer_file = resp.answer_file;
                                return _this.cache.exam.questions[curQuestion - 1] = cur_question
                            }
                        })
                    }
                })(this))
            };
            Controller.prototype.nextQuestion = function () {
                var dataPagebar, questions;
                questions = this.cache.exam.questions;
                if (!(this.current_question_index < questions.length)) {
                    return
                }
                this.view.render("showContent", questions[this.current_question_index]);
                this.current_question_index += 1;
                dataPagebar = {questions_count: questions.length, current_question_index: this.current_question_index};
                return this.view.render("showPagebar", dataPagebar)
            };
            Controller.prototype.prevQuestion = function () {
                var dataPagebar, questions;
                questions = this.cache.exam.questions;
                if (!this.current_question_index > 0) {
                    return
                }
                this.current_question_index -= 1;
                this.view.render("showContent", questions[this.current_question_index - 1]);
                dataPagebar = {questions_count: questions.length, current_question_index: this.current_question_index};
                return this.view.render("showPagebar", dataPagebar)
            };
            Controller.prototype._getExam = function (params, handler) {
                if (!isEmpty(this.cache.exam) && (this.cache.exam.chapter === params.chapter || this.cache.exam.id === params.id)) {
                    handler(this.cache.exam);
                    return
                }
                if (params.id) {
                    this.model.getExam(params.id, (function (_this) {
                        return function (resp) {
                            _this.cache.exam = resp;
                            return handler(_this.cache.exam)
                        }
                    })(this));
                    return
                }
                if (params.chapter) {
                    this.model.listExam(params.chapter, (function (_this) {
                        return function (resp) {
                            _this.cache.exam = resp.results[0];
                            return handler(_this.cache.exam)
                        }
                    })(this))
                }
            };
            Controller.prototype._getExamResult = function (id, handler) {
                if (!isEmpty(this.cache.examResult) && (this.cache.examResult.id = id)) {
                    handler(this.cache.examResult);
                    return
                }
                this.model.getExamResult(id, (function (_this) {
                    return function (resp) {
                        if (resp.status !== 200) {
                            topMsg("Ошибка: этот тест не начат. Через 3 секунды, тест начнется сначала.");
                            setTimeout(_this._redirectToStart, 3000);
                            return
                        }
                        _this.cache.examResult = resp.json;
                        return handler(_this.cache.examResult)
                    }
                })(this))
            };
            Controller.prototype._activateTimer = function (seconds) {
                var activateTimer, timeout;
                this.timer = seconds - 1;
                timeout = null;
                activateTimer = (function (_this) {
                    return function () {
                        return timeout = setTimeout(function () {
                            var finish;
                            _this.view.render("updateTimer", _this.timer);
                            if (_this.timer <= 0) {
                                return _this.sendAnswer(finish = true)
                            } else {
                                _this.timer--;
                                return activateTimer()
                            }
                        }, 1000)
                    }
                })(this);
                activateTimer();
                return cur.destroy.push(function () {
                    return clearTimeout(timeout)
                })
            };
            Controller.prototype._redirectToResult = function (resultId) {
                return window.location.replace("/ch/" + this.chapter + "/results/exam" + resultId)
            };
            Controller.prototype._redirectToStart = function () {
                return window.location.replace("/ch/" + this.chapter + "/exam")
            };
            Controller.prototype.sendAnswerBtnClick = function () {
                return this.sendAnswer()
            };
            Controller.prototype.prevQuestionBtnClick = function () {
                return this.prevQuestion()
            };
            Controller.prototype.nextQuestionBtnClick = function () {
                return this.nextQuestion()
            };
            Controller.prototype.answerInputKeyup = function (el) {
                if (trim(val(el)).length) {
                    return this.view.render("showSendBtn")
                } else {
                    return this.view.render("hideSendBtn")
                }
            };
            Controller.prototype.addAnswerFileBtnClick = function (el) {
                addClass(el, "__active");
                return ge("examAnswerFileInput").click()
            };
            Controller.prototype.startBtnClick = function () {
                return this.start()
            };
            return Controller
        })();
        window.Apps || (window.Apps = {});
        (_base = window.Apps).exam || (_base.exam = {});
        window.Apps.exam.Controller = Controller
    })(window);
    (function (window) {
        var Exam;
        Exam = (function () {
            function Exam(api_url) {
                this.s = new Apps.exam.Store(api_url);
                this.m = new Apps.exam.Model(this.s);
                this.t = new Apps.exam.Template();
                this.v = new Apps.exam.View(this.t);
                this.c = new Apps.exam.Controller(this.m, this.v)
            }

            return Exam
        })();
        window.Exam = Exam
    })(window);
    (function (window) {
        var Store, _base;
        Store = (function () {
            function Store(echo_url, callback) {
                this.echo_url = echo_url;
                if (callback == null) {
                    callback = function () {
                    }
                }
                callback.call(this);
                this.sock
            }

            Store.prototype.init = function (onOpen, onMessage) {
                this.sock = new SockJS(this.echo_url);
                this.sock.onmessage = onMessage;
                this.sock.onclose = (function (_this) {
                    return function () {
                        return topError("Ошибка: соединение потеряно. Попробуйте обновить страницу.")
                    }
                })(this);
                return this.sock.onopen = (function (_this) {
                    return function () {
                        if (_this.sock.readyState !== SockJS.OPEN) {
                            debugLog("chat: NOT connected")
                        } else {
                            debugLog("chat: connected")
                        }
                        return onOpen.call(_this, _this.sock)
                    }
                })(this)
            };
            Store.prototype.send = function (data) {
                return this.sock.send(JSON.stringify(data))
            };
            return Store
        })();
        window.Apps || (window.Apps = {});
        (_base = window.Apps).chat || (_base.chat = {});
        window.Apps.chat.Store = Store
    })(window);
    (function (window) {
        var Model, _base;
        Model = (function () {
            function Model(store) {
                this.store = store;
                this.sock
            }

            Model.prototype.init = function (onMessage) {
                return this.store.init((function (_this) {
                    return function (socket) {
                        return _this.sock = socket
                    }
                })(this), onMessage)
            };
            Model.prototype.msgCreate = function (msg) {
                msg = {text: msg};
                return this.store.send(msg)
            };
            return Model
        })();
        window.Apps || (window.Apps = {});
        (_base = window.Apps).chat || (_base.chat = {});
        window.Apps.chat.Model = Model
    })(window);
    (function (window) {
        var Template, _base;
        Template = (function () {
            function Template(storage) {
                this.storage = storage;
                this.msgTemplate = '{% for msg in it.message_list %}\n  <div class="chat_messages_i" id="msg{{ msg.id }}">\n      <div class="chat_messages_i_date">{{ msg.created }}</div>\n      <div class="chat_messages_i_name"\n        onclick="app.chat.c.msgUsernameClick(this);"\n        >[{{ msg.username }}]</div>\n      <div class="chat_messages_i_txt">\n          {{ msg.text }}\n      </div>\n      <div class="clearfix"></div>\n  </div>\n{% endfor %}';
                this.contactTeamplate = '{% for c in it.contact_list %}\n  <div class="sidebar_contacts_i" id="contact_{{ c.username }}">\n      <a class="sidebar_contacts_i_ava" href="/users/{{ c.username }}">\n          <img class="sidebar_contacts_i_ava_img"\n               src="{{ c.avatar }}"/>\n      </a>\n\n      <a class="sidebar_contacts_i_mail"\n         href="/pm/new/{{ c.username }}">\n          <i class="mic-mail"></i>\n      </a>\n\n      <div class="sidebar_contacts_i_body">\n          <a class="sidebar_contacts_i_name"\n            href="/users/{{ c.username }}"\n            >{{ c.username }}</a>\n          {% if c.about %}\n            <div class="sidebar_contacts_i_desc">{{ c.about }}</div>\n          {% endif %}\n      </div>\n  </div>\n{% endfor %}'
            }

            Template.prototype.showMessages = function (data) {
                var tmpl;
                tmpl = tpl.template(this.msgTemplate);
                data = this.stripText(data);
                data = this.replaceLinks(data);
                data = this.addBr(data);
                return cf(tmpl({message_list: data}))
            };
            Template.prototype.showContacts = function (data) {
                var tmpl;
                tmpl = tpl.template(this.contactTeamplate);
                return cf(tmpl({contact_list: data}))
            };
            Template.prototype.addBr = function (data) {
                var obj, _i, _len;
                for (_i = 0, _len = data.length; _i < _len; _i++) {
                    obj = data[_i];
                    obj.text = str_replace("\n", "<br/>", obj.text)
                }
                return data
            };
            Template.prototype.replaceLinks = function (data) {
                var obj, _i, _len;
                for (_i = 0, _len = data.length; _i < _len; _i++) {
                    obj = data[_i];
                    obj.text = app.utils.replaceURLWithHTMLLinks(obj.text)
                }
                return data
            };
            Template.prototype.stripText = function (data) {
                var obj, _i, _len;
                for (_i = 0, _len = data.length; _i < _len; _i++) {
                    obj = data[_i];
                    obj.text = stripHTML(obj.text)
                }
                return data
            };
            return Template
        })();
        window.Apps || (window.Apps = {});
        (_base = window.Apps).chat || (_base.chat = {});
        window.Apps.chat.Template = Template
    })(window);
    (function (window) {
        var View, _base;
        View = (function () {
            function View(template) {
                this.template = template;
                this.newLineMsgInput = __bind(this.newLineMsgInput, this);
                this.clearMsgInput = __bind(this.clearMsgInput, this);
                this.showEmoticonize = __bind(this.showEmoticonize, this);
                this.$body = ge("body");
                this.$page_body = ge("page_body");
                this.$msgList = ge("msgList");
                this.$msgInput = ge("msgInput");
                this.$contactList = ge("contactList");
                this.$notifyCount = ge("notifyCount")
            }

            View.prototype.showMessages = function (data) {
                append(this.$msgList, this.template.showMessages(data));
                return bodyNode.scrollTop += 10000
            };
            View.prototype.showContacts = function (data) {
                return append(this.$contactList, this.template.showContacts(data))
            };
            View.prototype.removeContact = function (username) {
                if (ge("contact_" + username)) {
                    return re("contact_" + username)
                }
            };
            View.prototype.showNotifyCount = function (count) {
                if (!count) {
                    return addClass(this.$notifyCount, "__hide")
                } else {
                    this.$notifyCount.innerHTML = count;
                    return removeClass(this.$notifyCount, "__hide")
                }
            };
            View.prototype.addUserReply = function (username) {
                this.clearMsgInput();
                return this.$msgInput.value = username
            };
            View.prototype.showEmoticonize = function (el_id) {
                var el;
                el = ge("msg" + el_id);
                return el.innerHTML = emoticonize(el.innerHTML, true)
            };
            View.prototype.clearMsgInput = function () {
                return this.$msgInput.value = ""
            };
            View.prototype.newLineMsgInput = function () {
                return this.$msgInput.value += "\n"
            };
            View.prototype.getMessageData = function (handler) {
                var msg;
                msg = trim(this.$msgInput.value);
                if (msg.length) {
                    handler(msg)
                } else {
                    return topMsg("Ошибка: эм, а ты сообщение ввести не забыл?", 3)
                }
            };
            return View
        })();
        window.Apps || (window.Apps = {});
        (_base = window.Apps).chat || (_base.chat = {});
        window.Apps.chat.View = View
    })(window);
    (function (window) {
        var Controller, _base;
        Controller = (function () {
            function Controller(model, view) {
                this.model = model;
                this.view = view;
                this.onMessage = __bind(this.onMessage, this);
                this.cache = {chat: {}};
                nav.add(/^$/, (function (_this) {
                    return function () {
                        return _this.init()
                    }
                })(this))
            }

            Controller.prototype.init = function () {
                if (yan.user_is_auth) {
                    return this.model.init(this.onMessage)
                }
            };
            Controller.prototype.onMessage = function (e) {
                var data, res, type;
                res = e.data;
                if (!res) {
                    return
                }
                type = res.type;
                data = res.data;
                console.log(res.type);
                switch (type) {
                    case"new_message":
                        return this.showMessage([data]);
                    case"list_messages":
                        return this.showMessages(data);
                    case"list_contacts":
                        return this.showContacts(data);
                    case"new_contact":
                        return this.showContacts([data]);
                    case"remove_contact":
                        return this.removeContact(data);
                    case"changed_notify_count":
                        return this.change_notify_count(data)
                }
            };
            Controller.prototype.showMessage = function (data) {
                return this.view.showMessages(data)
            };
            Controller.prototype.showMessages = function (data) {
                return this.view.showMessages(data)
            };
            Controller.prototype.removeContact = function (username) {
                return this.view.removeContact(username)
            };
            Controller.prototype.showContact = function (data) {
                return this.view.showContacts(data)
            };
            Controller.prototype.showContacts = function (data) {
                return this.view.showContacts(data)
            };
            Controller.prototype.sendAnswer = function () {
                return this.view.getMessageData((function (_this) {
                    return function (msg) {
                        _this.model.msgCreate(msg);
                        return _this.view.clearMsgInput()
                    }
                })(this))
            };
            Controller.prototype.change_notify_count = function (count) {
                return this.view.showNotifyCount(count)
            };
            Controller.prototype.msgBtnSendClick = function () {
                return this.sendAnswer()
            };
            Controller.prototype.msgInputKeyDown = function (e) {
                var ev;
                ev = e || window.event;
                if (ev.keyCode === 10 || ev.keyCode === 13 && (ev.ctrlKey || ev.metaKey && browser.mac)) {
                    return this.view.newLineMsgInput()
                } else {
                    if (ev.keyCode === 13) {
                        this.sendAnswer();
                        return cancelEvent(e)
                    }
                }
            };
            Controller.prototype.msgUsernameClick = function (el) {
                var username;
                username = el.innerHTML;
                username = str_replace("[", "", username);
                username = str_replace("]", "", username);
                return this.view.addUserReply("" + username + ", ")
            };
            return Controller
        })();
        window.Apps || (window.Apps = {});
        (_base = window.Apps).chat || (_base.chat = {});
        window.Apps.chat.Controller = Controller
    })(window);
    (function (window) {
        var Chat;
        Chat = (function () {
            function Chat(echo_url) {
                this.s = new Apps.chat.Store(echo_url);
                this.m = new Apps.chat.Model(this.s);
                this.t = new Apps.chat.Template();
                this.v = new Apps.chat.View(this.t);
                this.c = new Apps.chat.Controller(this.m, this.v)
            }

            return Chat
        })();
        window.Chat = Chat
    })(window);
    (function () {
        var Run, chatApp, examApp, utilsApp;
        utilsApp = new Utils();
        examApp = new Exam("/api/v1/");
        chatApp = new Chat("http://" + yan.chat.host + ":" + yan.chat.port + "/" + yan.chat.channel);
        Run = function () {
            nav.init()
        };
        Run();
        window.app = window.app || {};
        window.app.utils = utilsApp;
        window.app.chat = chatApp
    })()
}).call(this);